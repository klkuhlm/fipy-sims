from fipy import CellVariable, FaceVariable, TransientTerm, DiffusionTerm, Viewer
from fipy.meshes.nonUniformGrid2D import NonUniformGrid2D
import numpy as np
import matplotlib.pyplot as plt

FT2M = 0.3048

Lx = 25.0 *FT2M 
Ly = 70.0 *FT2M

nTimeSteps = 300

# thermal conductivity [W/(m deg-C)] parameters
# kappa = a0 + a1*T + a2*T**2 + a3*T**3

##a = [5.734, -1.838E-2, 2.86E-5, -1.51E-8]

# volumetric heat capacity: [J/(m^3 deg-C)]
# c_{th,rs}*rho_{th,rs} = b0 + b1*T 

##b = [1.8705E+6, 3.8772E+2]

# grid spacing
dy = (2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,
      1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0, 0.5,0.5,
      0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
      0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
      0.5,0.5, 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,
      2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2)

dx = (0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
      0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
      0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
      0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
      0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,
      1.0,1.0,1.0,1.0,1.0)

f = lambda x:x*FT2M
dx = list(map(f,dx))
dy = list(map(f,dy))

m = NonUniformGrid2D(dx=dx, dy=dy)
x,y = m.cellCenters.value

# geometry
drift   =  (y>30*FT2M) & (y<40*FT2M)
# y = 52 - 2 is diagonal line from (14,38) to (22,30)
saltPile = (y>30*FT2M) & (y<38*FT2M) & (y<52*FT2M-x)  

htr1 = (x<1*FT2M)              & (y>30*FT2M) & (y<32*FT2M)
htr2 = (x>2*FT2M) & (x<4*FT2M) & (y>30*FT2M) & (y<32*FT2M)
htr3 = (x>5*FT2M) & (x<7*FT2M) & (y>30*FT2M) & (y<32*FT2M)

T = CellVariable(name='temperature', mesh=m, value=25.0) #, hasOld=1)

T.setValue(15.0, where=drift)
T.setValue(35.0, where=saltPile)
T.setValue(50.0, where=htr1)
T.setValue(60.0, where=htr2)
T.setValue(70.0, where=htr3)

Q = CellVariable(name='source', mesh=m, value=0.0)

heaterQ = 500.0  # W/m^2

# heaters (square cross-sections)
Q.setValue(heaterQ, where=htr1)
Q.setValue(heaterQ, where=htr2)
Q.setValue(heaterQ, where=htr3)

# insulating/symmetry BC around domain (no need to specify)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# density [g/m^3]

intactSaltDensity = 2.0700
crushedSaltDensity = 1.5
sandDensity = 2.330
airDensity = 1.2E-3

# specific heat capacity (per mass) * density = volumetric heat capacity (per volume)

# specific heat capacity [J/(g*deg-C)]

intactSaltRhocp = intactSaltDensity*0.88
crushedSaltRhocp = crushedSaltDensity*0.88/5.0
sandRhocp = sandDensity*0.8
airRhocp = airDensity*1.012

# volumetric heat capacity [J/(m^3 deg-C)]
rhocp = CellVariable(mesh=m, value=intactSaltRhocp) 

rhocp.setValue(airRhocp, where=drift)

# rectangular pile for simplicity
rhocp.setValue(crushedSaltRhocp, where=saltPile)

# sand-filled heaters (~glass logs)
rhocp.setValue(sandRhocp, where=htr1)
rhocp.setValue(sandRhocp, where=htr2)
rhocp.setValue(sandRhocp, where=htr3)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# http://en.wikipedia.org/wiki/List_of_thermal_conductivities
# BAMBUS II report p. 45 (Figure 2.37)

intactSaltKap = 5.7
crushedSaltKap = 1.0 # 30% porosity
sandKap = 1.1 # borosilicate glass (from Phil 7/10/13)
airKap = 14.0 #  <- effective thermal conductivity including approximate radiation and convection (Phil 7/10/13)
# airKap = 0.0262  #(1 atm, 300K)   <- conduction only

# thermal conductivity [W/(m deg-C)]
kappa = CellVariable(mesh=m, value=intactSaltKap) 

kappa.setValue(airKap, where=drift)

# rectangular pile for simplicity
kappa.setValue(crushedSaltKap, where=saltPile)

# sand-filled heaters (~glass logs)
kappa.setValue(sandKap, where=htr1)
kappa.setValue(sandKap, where=htr2)
kappa.setValue(sandKap, where=htr3)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# heat conduction governing equation
#eq = (TransientTerm(b[0] + b[1]*T, var=T) == 
#      DiffusionTerm(a[0] + a[1]*T + a[2]*T**2 + a[3]*T**3, var=T) + Q)

eq = (TransientTerm(rhocp, var=T) == DiffusionTerm(kappa, var=T) + Q)

viewer = Viewer(vars=(T),datamin=25.0,datamax=200)
viewer.plot()
#plt.axis('equal')

t = 0.0

for j,step in enumerate(np.logspace(-3,0,nTimeSteps)):
    t += step
    eq.solve(dt=step)
    #T.updateOld()

    #res = 1000.0
    #while res > 1.0E-4:
    #    #print('res',res)
    #    res = eq.sweep(dt=step)
   
    if j%5 == 0:
        print("%i: %.1f" % (j,t))
        viewer.plot()


