from fipy import CylindricalGrid1D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, ExponentialConvectionTerm, Viewer
from fipy import numerix as fn
import matplotlib.pyplot as plt
import numpy as np

L = 100.0
nx = 200
dx = L/nx
dt = 0.5
nsteps = 1000

writeOutputFile = False
outlocs = [0,49,69]

# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# set boundary conditions
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# type 1,2, or 3 BC for concentration at the wellbore
# dc/dr = dc/dt is "4"

# the following flow/conc BC combinations "make sense"
# 1 with injection
# 2 with extraction
# 3 with injection
# 4 with extraction 

wellboreConcBC = 2

# extraction or injection
injection = False

# type 1 or 2 BC for concetration at far edge
farConcBC = 1
# ////////////////////////////////////////


# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# set properties
# \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
phi = 0.01        # porosity
perm = 1.0E-9     # permeability (m^2)
betab = 1.0E-9    # bulk compressibility (1/Pa) 
D = 1.0E-9        # diffusion coeff (m^2/sec)
alpha = 1.0       # longitudinal dispersivity (m)
R = 1.0           # retardation coefficient (-)
# ////////////////////////////////////////

DR = D/R
rw = 1.0E-4       # wellbore radius (m)
b = 0.01           # aquifer thickness (m)
A = 2.0*np.pi*rw*b*phi  # area for advection out of well screen (m^2)
muW = 1.0         # pumped well "mixing factor" (Chen et al., 2002)
Pe = 1.0          # Peclet number (used in laplace-space Type-III sol'n)

etaw = 8.9E-4     # kinematic viscosity (Pa*sec = N*sec/m^2 = kg/(m*sec))
rhow = 999.97     # density of water (kg/m^3)
g = 9.81          # gravitational acceleration (m/sec^2)
gamw = rhow*g     # specific weight of water (kg/(m^2*sec^2))
K = perm*gamw/etaw   # hydraulic conductivity (m/sec)

print 'hydraulic conductivity: (m/s)  ',K
print 'hydraulic conductivity: (m/day)',K*60*60*24

betaw = 4.6E-10   # compressibility of water (m^2/N = m*sec^2/kg = 1/Pa)
Ss = gamw*(betaw*phi + betab)   # volumetric specific storage (1/m)
print 'specific storage: (1/m)',Ss

m = CylindricalGrid1D(dx=dx, nx=nx)
m.origin = (rw) 

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# flow solution to diffusion equation

h = CellVariable(name='$h$',mesh=m, value=0.0)

# specified flux at well/ constant head at other
rate = 5.0E+3
if injection:
    Q = -rate
else:
    Q = rate

h.constrain(0.0, m.facesRight)  # no drawdown at far edge
h.faceGrad.constrain(Q, m.facesLeft)  # specified flowrate at well

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# concentration transport advection/dispersion equation

# non-zero initial concentration section of domain
C0 = 1.0
cinit = np.zeros(nx, 'd')

if not injection:
    cinit[0:40] = np.linspace(C0,0.0,40)

c = CellVariable(name="$c$", mesh=m, value=cinit, hasOld=True)

if wellboreConcBC == 1:
    # concentration boundary condition at well
    print 'non-homogeneous Type I concentration BC at well'
    c.constrain(C0, m.facesLeft)   
    if not injection:
        # Type I BC only makes sense for injection??
        print "*****\nWARNING: Type I BC at well for extraction\n*****"

elif wellboreConcBC == 2:
    print 'homogeneous Type II concentration BC at well'
    c.faceGrad.constrain(0.0, m.facesLeft)  
    if injection:
        # Type II BC only makes sense for extraction??
        print "*****\nWARNING: Type II BC at well for injection\n*****"

elif wellboreConcBC == 3:
    print 'non-homogeneous Type III BC at well (Chen, 1987)'
    # comes from mass balance at wellbore (no molecular diffusion)
    # mass in = Q*C0
    # advected mass out = A*v*c (@ rw)
    # dispersion mass out = A*alpha*v*dc/dr (@ rw)
    # v = -K/phi * dh/dr  (pore velocity)
    # but Q specified above is Q/A already (m/sec), not (m^3/sec)
    c.faceGrad.constrain(1/alpha*(c.faceValue - 
                          Q*C0*phi/(h.faceGrad*K)), m.facesLeft)
    if not injection:
        # Type III BC only makes sense for injection??
        print "*****\nWARNING: Type III BC at well for extraction\n*****"

elif wellboreConcBC == 4:
    print 'homogeneous Laplace-space Type III BC (Moench, 1989), as implemented by Chen et al. (2002)'
    c.faceGrad.constrain(muW/Pe*(c - c.old)/dt, m.facesLeft)
    if injection:
        # Laplace-space Type III only makes sense for extraction??
        print "*****\nWARNING: Laplace-space Type III BC at well for injection\n*****"

else:
    print 'invalid wellbore boundary condition', wellboreConcBC

if farConcBC == 1:
    c.constrain(0.0, m.facesRight)
elif farConcBC == 2:
    c.faceGrad.constrain(0.0, m.facesRight)
else:
    print 'invalid far BC for concentration problem',farConcBC

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# governing flow equation
Heq = TransientTerm(Ss,var=h) == DiffusionTerm(K,var=h) 

# governing transport equation
Ceq = (TransientTerm(phi,var=c) == 
       phi*DiffusionTerm((alpha*fn.absolute(h.grad[0]*(K/phi)) + D),var=c) +
       ExponentialConvectionTerm(h.faceGrad*K,var=c))

# convection term expects a faceVariable as a parameter
# while diffusion term expects a cellVariable as a parameter
# .grad returns a cellVariable, while .faceGrad returns a faceVariable 

Hviewer = Viewer(vars=(h,))
Hviewer.plot()

Cviewer = Viewer(vars=(c,),datamin=0,datamax=1)
Cviewer.plot()

time = 0.0

if writeOutputFile:
    fh = open('radial-flow-and-transport.dat','w')
    fmtstr = ','.join('%.5f' for i in range(len(outlocs)*4+1)) + '\n'

for j in range(nsteps):
    
    time += dt
    print '%5i:%5.2f' % (j,time)

    Heq.solve(dt=dt)
    Hviewer.plot()

    if writeOutputFile:
        out = [time,]
        out.extend([h.getValue()[i] for i in outlocs])

    c.updateOld()
    Ceq.solve(dt=dt)
    Cviewer.plot()

    if writeOutputFile:
        out.extend([c.getValue()[i] for i in outlocs])
        fh.write(fmtstr % tuple(out))
