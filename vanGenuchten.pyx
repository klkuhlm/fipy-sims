# need to compile Cython module before using
# python setup.py build_ext --inplace

#cython: boundscheck=False, cdivision=True

import numpy as np
cimport numpy as np
DTYPE = np.double
ctypedef np.double_t DTYPE_t

def calc_C(np.ndarray[DTYPE_t] x, double n, double m, double alpha):
    # ((m-1) n (alpha (-x))^n ((alpha (-x))^n+1)^(m-2))/x
    return ((m-1)*n*(-alpha*x)**n * ((-alpha*x)**(n+1))**(m-2))/x
   
def calc_Se(np.ndarray[DTYPE_t] x, double n, double m, double alpha):
    return (1.0 + (-alpha*x)**n)**(-m)

def calc_K(np.ndarray[DTYPE_t] x, double p, double m):
    return x**p*(1.0 - (1.0 - x**(1.0/m))**m)**2
   
