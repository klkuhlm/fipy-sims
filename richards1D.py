from fipy import Grid1D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, Viewer
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import vanGenuchten as mrc
alpha = 0.5
m = 0.75
n = 1.0/(1.0 - m)
p = 0.5
name = "Mualem/van Genuchten"

#import BrooksCorey as mrc

# fipy script implementing Richards equation for unsaturated vertical flow.
# Gravity acts "down" is in direction of increasing x (like depth).

# Brooks-Corey model (1) sort of works, but has issues near saturation
# van Genuchten model (3) is more robust.

# capillary pressure models taken from:
# Warrick (2003) "Soil Water Dynamics", Table 2-1

save_figures = True

L = 10.0
nx = 200
dx = L/nx

# since this is sort of like an explicit convection (of moisture by gravity)
# solution (i.e., .divergence() term in PDE), timeStep must be kept small for large K
timeStep = 1.0  # seconds 
num_time_steps = 3000


mesh = Grid1D(dx=dx, nx=nx)
x_cc = mesh.cellCenters.value[0]
x_fc = mesh.faceCenters.value[0]

# saturated/common properties
# could make this heterogeneous by making it a FaceVariable
Ks = 1.0E-2     # saturated K [m/sec]

# rather dry "background"
h0 = -10.0
h = CellVariable(name="$\psi$", mesh=mesh, value=h0, hasOld=True) # [m] capillary head

# initial condition with middle section wetter than background (but not saturated)
h.setValue(-1.5, where=(x_cc > 3.0)&(x_cc < 5.0)) 

# to specify initial condition in terms of saturation, need h(Se) implemented for each model

C = CellVariable(name="$C(h)$", mesh=mesh, value=0.0) # moisture capacity 
K = FaceVariable(name="$K(S_e)$", mesh=mesh, value=0.0) # hydraulic conductivity
Se = FaceVariable(name="$S_e(h)$", mesh=mesh,value=0.0) # saturation

# negative flux on left side means flow into domain
#h.faceGrad.constrain(-5.0*Ks, mesh.facesLeft)  # flux into top of domain
h.constrain(-1.0, mesh.facesRight) # water table condition at bottom

# %%%%% define functional forms of capillary pressure models

# when a FaceVariable (nx+1) is needed from a CellVariable (nx)
# .arithmeticVaceValue is simpler
# .harmonicFaceValue is maybe better
   
C.setValue(mrc.calc_C(h,n,m,alpha))
Se.setValue(mrc.calc_Se(h.arithmeticFaceValue,n,m,alpha))
K.setValue(mrc.calc_K(Se,p,m)*Ks)
   
# Richards' equation
g = [1] # gravity "vector" (not really needed in 1D)
Req = (TransientTerm(coeff=C, var=h) == DiffusionTerm(coeff=K, var=h) - (K*g).divergence)

if not save_figures:
   viewerh = Viewer(vars=h)
   viewerh.plot()
   viewerC = Viewer(vars=C)
   viewerC.plot()
else:
   plt.figure(1,figsize=(12,5))
   plt.subplot(131)
   hplot = -numpy.logspace(-2,2,200) # negative head
   Seplot = mrc.calc_Se(hplot,n,m,alpha)
   plt.semilogy(Seplot,-hplot)
   plt.xlabel('$S_e$')
   plt.ylabel('$-h$')
   plt.grid()

   plt.subplot(132)
   Kplot = mrc.calc_K(Seplot,p,m)
   plt.loglog(Kplot,-hplot)
   plt.xlabel('$K$')
   plt.ylabel('$-h$')
   plt.grid()

   plt.subplot(133)
   Cplot = mrc.calc_C(hplot,n,m,alpha)
   plt.loglog(Cplot,-hplot)
   plt.xlabel('$C$')
   plt.ylabel('$-h$')
   plt.grid()
   plt.suptitle(name)
   plt.tight_layout()
   plt.savefig('check-richards-functions.png')
   plt.close(1)

t = 0.0
   
for step in range(num_time_steps):

   t += timeStep
   h.updateOld()

   res = 100.0
   j = 0
   while res > 1.0E-5 and j < 50:
      # update mrc within non-linear iteration
      C.setValue(mrc.calc_C(h,n,m,alpha))
      Se.setValue(mrc.calc_Se(h.arithmeticFaceValue,n,m,alpha))
      K.setValue(mrc.calc_K(Se,p,m)*Ks)
      
      res = Req.sweep(dt=timeStep,var=h)
      j += 1

   if j == 50:
      print('**WARNING: non-linear Picard iteration did not converge**')
   print(step,'(',j,res,')',t)

   C.setValue(mrc.calc_C(h,n,m,alpha))
   Se.setValue(mrc.calc_Se(h.arithmeticFaceValue,n,m,alpha))
   K.setValue(mrc.calc_K(Se,p,m)*Ks)
   
   if not save_figures:
      viewerh.plot()
      viewerC.plot()
   else:
      if step%40 == 0:
         plt.figure(2,figsize=(16,5))
         plt.subplot(141)
         plt.plot(Se.value,x_fc)
         plt.xlabel('$S_e$')
         plt.ylabel('$z$')
         plt.ylim([L,0])
         plt.grid()

         plt.subplot(142)
         plt.semilogx(K.value,x_fc)
         plt.xlabel('$K$')
         plt.ylabel('$z$')
         plt.ylim([L,0])
         plt.grid()

         plt.subplot(143)
         plt.plot(C.value,x_cc)
         plt.xlabel('$C$')
         plt.ylabel('$z$')
         plt.ylim([L,0])
         plt.grid()

         plt.subplot(144)
         plt.semilogx(-h.value,x_cc)
         plt.xlabel('$-h$')
         plt.ylabel('$z$')
         plt.ylim([L,0])
         plt.grid()

         plt.tight_layout()
         plt.savefig('richards-soln-%4.4i.png' % step)
         plt.close(2)
