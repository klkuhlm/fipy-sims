from fipy import CylindricalGrid2D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, Viewer, numerix


L = 30.0 # radial length of domain (m)
b = 0.25 # thickness of a layer (m)
nr = 150
dr = L/nr

nz = 15
dz = b/nz

timeStep = 0.025 # dx/10.0
steps = 100

Ss = 2.0E-4 # 1/m
Kr = 4.5E-4 # m/s
Sy = 0.3
S = Ss*b
T = Kr*b

Q = 1.0E0

sigma = 1.0E-1 # S/m  
C = -13.00 # mV/m

# electrical problem is two layers (bottom one overlaps with flow problem
m = CylindricalGrid2D(dx=dr, nx=nr, ny=3*nz, dy=dz)

m.origin = numerix.array([[1.0E-4],
                          [0.0]])

s = CellVariable(name='drawdown',mesh=m, value=0.0)
phi = CellVariable(name='voltage',mesh=m, value=0.0)

# specify flow properties of "aquitard" are very small (~no flow)
Tmat = FaceVariable(mesh=m, value=T)
z = m.getFaceCenters()[1]
Tmat.setValue(T/1000.0, where=(z > 2*b))
Tmat.setValue(T/1000.0, where=(z < b))

# make top layer of aquifer unconfined storage
Smat = FaceVariable(mesh=m, value=S)
#Smat.setValue(Sy, where=((z > 2*b - b/5.0) & (z < 2*b + 0.01)))

sigmamat = FaceVariable(mesh=m, value=sigma)
sigmamat.setValue(sigma/10, where=(z > 2*b))
sigmamat.setValue(sigma*10, where=(z < b))

# only pump from the middle layer
pumpingRate = Q/(2.0*numerix.pi*T)
aquiferWell = m.facesLeft & (z > b) & (z < 2*b)

s.faceGrad.constrain(pumpingRate,aquiferWell)
s.constrain(0.0, m.facesRight)

phi.faceGrad.constrain(-pumpingRate*C,aquiferWell)
phi.constrain(0.0, m.facesRight)

eqgw = (DiffusionTerm(Tmat,var=s) == TransientTerm(S,var=s))
eqel = (DiffusionTerm(sigmamat,var=phi) == TransientTerm(C*sigma,var=s))

eq = eqgw & eqel

viewer1 = Viewer(vars=(s,phi))
viewer1.plot()

time = 0.0

for step in range(steps):
    time += timeStep

    print step,time

    eq.solve(dt=timeStep)
    viewer1.plot()
