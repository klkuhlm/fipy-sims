# implement EOS for density and viscosity used in SUTRA benchmakr against elder problem

def water_density(T, P):
    return 1.0E+3

def drhow_dp(T, P):
    return 0.0

def drhow_dT(T, P):    
    return 0.0

def drhob_dp(T, P, S):
    return 0.0

def drhob_dT(T, P, S):
    return 0.0

def drhob_dS(T, P, S):
    return 751.879699248 #200.0/0.266

def brine_density(T, P, S):
    return 1.0E+3 + 751.879699248*S #200.0*S/0.266

def viscosity(T, S):
    return 1.0E-3


