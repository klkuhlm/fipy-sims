# implement EOS for density and viscosity according to Adams & Bachu (2002) with erratum

# http://docs.cython.org/src/userguide/numpy_tutorial.html

# need to compile Cython module before using
# python setup.py build_ext --inplace

#cython: boundscheck=False, cdivision=True

import numpy as np
cimport numpy as np
DTYPE = np.double
ctypedef np.double_t DTYPE_t

# Batzle & Wang (1992), as Described in Adams & Bachu (2002).
def water_density(np.ndarray[DTYPE_t] T, np.ndarray[DTYPE_t] P):
    """equation 10 of A&B (2002)
    T in degrees C
    P in Pa (_NOT_ MPa)
    return: rho_w in kg/m^3 (_NOT_ g/cm^3)"""

    cdef np.ndarray[DTYPE_t] p = P*1.0E-6 # convert Pa -> MPa
    
    cdef np.ndarray[DTYPE_t] T2 = T*T
    cdef np.ndarray[DTYPE_t] T3 = T2*T
    cdef np.ndarray[DTYPE_t] p2 = p*p
    
    return (1.0E+3 + 1.0E-3*(-80.0*T - 3.3*T2 + 0.00175*T3 +
                             489.0*p - 2.0*T*p + 0.016*T2*p - 1.3E-5*T3*p -
                             0.333*p2 - 0.002*T*p2))

def brine_density(np.ndarray[DTYPE_t] T, np.ndarray[DTYPE_t] P, np.ndarray[DTYPE_t] S):
    """equation 11 of A&B (2002)
    T in degrees C
    P in Pa (_NOT_ MPa)
    S in ppm/10^6 (NaCl mass fraction)
    return: rho_B in kg/m^3 (_NOT_ g/cm^3)"""

    cdef np.ndarray[DTYPE_t] p = P*1.0E-6  # convert Pa -> MPa
    cdef np.ndarray[DTYPE_t] rhow = water_density(T,P)
    
    return rhow + (S*(0.668 + 0.44*S + 1.0E-6*(300.0*p - 2400.0*p*S +
                  T*(80.0 + 3.0*T - 3300.0*S - 13.0*p + 47.0*p*S))))*1.0E+3

def viscosity(np.ndarray[DTYPE_t] T, np.ndarray[DTYPE_t] S):
    """equation 19 of A&B (2002) -- w/ erratum
    T in degrees C
    P dependence not included
    S in ppm/10^6
    return: mu_B in Pa-sec (_NOT_ cP)"""

    cdef np.ndarray[DTYPE_t] A
    
    A = (0.42*(np.power(S,0.8) - 0.17)**2 + 0.045)*np.power(T,0.8)
    
    return (0.1 + 0.333*S + (1.65 + 91.9*S**3)*np.exp(-A))*1.0E-3


