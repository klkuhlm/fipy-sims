from fipy import Grid2D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, ExponentialConvectionTerm, Viewer
import matplotlib.pyplot as plt
import numpy 

# 1D cartesian pore (slit)
# stokes flow is given by steady-state distribution (parabola)
# nernst-planck equation for transport of ions
# poisson equation for electrostatics

# slit pore with flow in x direction and
# and pore width is h in y direction

dx,dy = (0.01, 0.01)
nx,ny = (100, 10)
dt = 0.0025

h = ny*dy # pore width
L  = nx*dx # pore length

zeta = 0.025 # surface potential (V)
D = 1.0E-9  # diffusion coefficient (m^2/sec)
Umax = 1.0 # max flow velocity (m/sec)

zp = +1.0  # ion valence for simple symmetric salt (+1/-1) 
zm = -1.0
z = zp   

e = 1.6021766E-19  # elementary charge on electron (Coulombs)
Na = 6.022141E+23  # Avagadro's number (1/mole)
F = e*Na  # Faraday constant (Coulombs/mole)
print 'Faraday',F,'C/mole'

R = 8.31446  # gas constant (Volts*Coulombs/Kelvin/mole = Joule/Kelvin/mole)
T = 298.15 # universal temperature [25 degrees C] (Kelvin) 
kB = R/Na # Boltzmann constant (V*C/K = J/K)
print 'Boltzmann',kB,'J/K'

epsilon0 = 8.854187817E-12 # Vacuum permittivity (Farads/meter)
epsilonr = 80.0 # relative permittivity of water
epsilon = epsilon0*epsilonr
print 'permittivity water',epsilon,'F/m'

# far-stream concentration (assumed symmetric 1:1 or 2:2, etc.)
ninf = 1.0E-3

# debye length 
kappa = numpy.sqrt(2*F*e*z**2*ninf/(epsilon*R*T))
print 'Debye length',kappa,'1/m'

# characteristic potential
PsiC = R*T/(z*F)
print 'Psi_C',PsiC,'V'

m = Grid2D(dx=dx, nx=nx, dy=dy, ny=ny)

# electric potential
psi = CellVariable(name='$\\psi$', mesh=m, value=0.0, hasOld=True)

# specified potential along Stern plane
psi.constrain(zeta, m.facesTop)
psi.constrain(zeta, m.facesBottom)

psi.constrain(0.0, m.facesLeft)
psi.faceGrad.constrain(0.0, m.facesRight)

# number concentration of ions
np = CellVariable(name='$n_+$', mesh=m, value=ninf, hasOld=True)
nm = CellVariable(name='$n_-$', mesh=m, value=ninf, hasOld=True)

np.constrain(ninf, m.facesLeft)
np.constrain(ninf, m.facesRight)
np.faceGrad.constrain(0.0, m.facesTop)
np.faceGrad.constrain(0.0, m.facesBottom)
nm.constrain(ninf, m.facesLeft)
nm.constrain(ninf, m.facesRight)
nm.faceGrad.constrain(0.0, m.facesTop)
nm.faceGrad.constrain(0.0, m.facesBottom)

Poisson = DiffusionTerm(coeff=epsilon, var=psi) == -e*(zp*np + zm*nm)

NernstPlanckp = (DiffusionTerm(coeff=D, var=np) -
                 DiffusionTerm(coeff=e*D*zp/(kB*T)*np, var=psi) == 0.0)

NernstPlanckm = (DiffusionTerm(coeff=D, var=nm) -
                 DiffusionTerm(coeff=e*D*zm/(kB*T)*nm, var=psi) == 0.0)

Eqns = Poisson & NernstPlanckp & NernstPlanckm

viewer = Viewer(vars=(psi,np,nm))

#psi.updateOld()
#np.updateOld()
#nm.updateOld()

Eqns.solve()
##res = 1.0E+4
##j = 0
##while res < 1.0E-4:
##    res = Eqns.sweep()
##    j += 1
##    print j,res
viewer.plot()

