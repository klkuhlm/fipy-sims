from fipy import *

L = 10.0
nx = 100
dx = L/nx

#m = Grid1D(nx=nx, dx=dx)                          # works with this grid
m = CylindricalGrid1D(nx=nx, dx=dx) # fails with this grid

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#c0 = numerix.zeros(nx, 'd')
#c0[40:60] = 1.0
c0 = numerix.linspace(0.0,1.0,nx)

cf = CellVariable(name="$c_f$", mesh=m, value=c0)  
cf.constrain(0.0, m.facesLeft)
cf.faceGrad.constrain([0.0,], m.facesRight)

CeqF = (TransientTerm(1.0,var=cf) == DiffusionTerm(0.25,var=cf) #)
        - ExponentialConvectionTerm((1.0,),var=cf))

Cviewer = Viewer(vars=(cf),datamax=1.0,datamin=-0.05)
Cviewer.plot()

timeStep = 3.0E-2
for j in range(200):
    CeqF.solve(dt=timeStep)
    Cviewer.plot()

