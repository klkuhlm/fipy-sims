import fipy as fp
import numpy as np
from scipy.integrate import cumtrapz
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import batzle_wang as bw  # cythonized version

save_figs = True

Lx = 6000.0
nx = 200
dx = Lx/nx

m = fp.Grid1D(dx=dx, nx=nx)
zc = m.cellCenters.value[0] # for cell-centered variables
zf = m.faceCenters.value[0] # for face-centered fluxes

T = fp.CellVariable(name='$T$', mesh=m, value=0.0, hasOld=True)
p = fp.CellVariable(name='$p$', mesh=m, value=0.0, hasOld=True)
S = fp.CellVariable(name='$X_{NaCl}$', mesh=m, value=1.0E-4, hasOld=True)

# constant coefficients
# ========================================
g = 9.81 # m/s^2
patm = 1.01325E+5 # Pa
Dm = 1.0E-7 # m^2/sec (mass diffusivity -- really dispersivity + diffusion*porosity?)

# initial conditions
# ========================================
Tsurf = 21.1 # [C]
Tdeep_guess = 151.0 # [C]
T0 = np.linspace(Tsurf,Tdeep_guess,nx)  # [C]
T.setValue(T0)

# S has units of NaCl mass fraction [ppm*1.0E-6]
S0 = np.linspace(0.01,0.1,nx)
S.setValue(S0)
S.setValue(0.2,where=(zc > 3000.0) & (zc < 5000.0))

# density and viscosity used in flux/diffusion calcs
p.setValue(zc*997.0*g + patm)  # [Pa] initial guess with constant fresh density

# face variables that are variable on system state
# ========================================

rhof = fp.FaceVariable(mesh=m)
rhof.setValue(bw.brine_density(T.harmonicFaceValue.value,
                               p.harmonicFaceValue.value,
                               S.harmonicFaceValue.value))

mu = fp.FaceVariable(mesh=m)
mu.setValue(bw.viscosity(T.harmonicFaceValue.value,
                         S.harmonicFaceValue.value))

# parameters that may be variable in space
# ========================================

# [m^2] intrinsic permeability of granite
k = fp.FaceVariable(mesh=m, value=1.0E-16) 

# porosity of granite
phi = fp.CellVariable(mesh=m, value=0.02) 

# [W/(m*K)] bulk (wet) thermal conductivity of granite -- really a mixture
kT = fp.FaceVariable(mesh=m, value=2.8)

# [J/(m^3*K)] bulk heat capacity (density * specific heat) -- really a mixture
cg = 790.0 # J/(kg*K) granite specific heat
rhog = 2750.0 # kg/m^3 granite density
rhocb = fp.CellVariable(mesh=m, value=cg*rhog)

cf = 4200.0 # J/(kg*K) water specific heat (not a strong function of temperature)
rhocf = fp.FaceVariable(mesh=m, value=cf*rhof)

# [1/Pa] fluid compressibility
compress = fp.CellVariable(mesh=m, value=4.0E-10)

# [W/m^3] heat source term from radioactive decay for granite
qp = fp.CellVariable(mesh=m, value=2.82E-6)
qp.setValue(2.5, where=(zc > 3000.0) & (zc < 5000.0))

# boundary condition related
# ========================================
qr = 65.0E-3 # [W/m^2] regional heat flux from SMU map (apply at shallow or deep?)

T.constrain(Tsurf, m.facesLeft)  # "surface" temperature
T.faceGrad.constrain(qr/kT, m.facesRight)  # "deep" flux

p.constrain(patm, m.facesLeft) # atmospheric pressure at top
#p.faceGrad.constrain(rhof*g, m.facesRight)  # constant head, not pressure at bottom

# no boundary conditions (aside from natural ones) for S

# governing equations
# ========================================

# transient single-porosity porous media flow
peq = (fp.TransientTerm(coeff=compress*phi, var=p) ==
       fp.DiffusionTerm(coeff=k/mu, var=p) + (k*rhof*g/mu).divergence)

# available fipy convection terms
# ----------------------------------------
# VanLeerConvectionTerm
# ExponentialConvectionTerm
# CentralDifferenceConvectionTerm
# HybridConvectionTerm
# UpwindConvectionTerm
# PowerLawConvectionTerm

ConvectionTerm = fp.VanLeerConvectionTerm

# transient thermal convection
Teq = (fp.TransientTerm(coeff=rhocb, var=T) +
       ConvectionTerm(coeff=rhocf*k/mu*(p.faceGrad + rhof*g), var=T) ==
       fp.DiffusionTerm(coeff=kT, var=T) + qp)

# transient soulte convection
Seq = (fp.TransientTerm(coeff=phi, var=S) +
       ConvectionTerm(coeff=k/mu*(p.faceGrad + rhof*g), var=S) ==
       fp.DiffusionTerm(coeff=Dm, var=S))

eqns = peq & Teq & Seq

if not save_figs:
    pviewer = fp.Viewer(vars=(p,), datamin=patm, legend=None)
    Tviewer = fp.Viewer(vars=(T,), datamin=Tsurf*0.9, legend=None)
    Sviewer = fp.Viewer(vars=(S,), datamin=1.0E-5, legend=None) # ylog=True, 

    pviewer.plot()
    Tviewer.plot()
    Sviewer.plot()

t = 0.0
dt_vec = np.concatenate((np.logspace(3,np.log10(5.0E+4),50),
                         np.ones((500,))*5.0E+4,
                         np.ones((500,))*1.0E+5,
                         np.ones((500,))*5.0E+5),axis=0)

for j,dt in enumerate(dt_vec):

    t += dt
    T.updateOld()
    p.updateOld()
    S.updateOld()
    
    res = 1.0E+5
    i = 0
    while res > 1.0E-5 and i < 5:
        res = eqns.sweep(dt=dt)
        i += 1

        rhob = bw.brine_density(T.harmonicFaceValue.value,
                                p.harmonicFaceValue.value,
                                S.harmonicFaceValue.value)
        mub = bw.viscosity(T.harmonicFaceValue.value,
                           S.harmonicFaceValue.value)
        
        rhof.setValue(rhob)
        mu.setValue(mub)

        #print 'DEBUG',res,i

    if j%10==0:
        print j,'(',i,') dt=%.3g sec' % (dt,),'t=%.3g yrs' % (t/31556925.9747,)

        if save_figs:
            fig = plt.figure(1,figsize=(18,5))
            axT = fig.add_subplot(151)
            axS = fig.add_subplot(152)
            axP = fig.add_subplot(153)
            axrho = fig.add_subplot(154)
            axmu = fig.add_subplot(155)

            axT.plot(T.value,zc*1.0E-3,'r-')
            axT.set_ylim([Lx*1.0E-3,0])
            axT.set_xlabel('T [C]')
            axT.set_ylabel('Z [km]')
            axT.grid()

            axP.plot(p.value*1.0E-6,zc*1.0E-3,'b-')
            axP.set_ylim([Lx*1.0E-3,0])
            axP.set_xlabel('P [MPa]')
            axP.grid()

            axS.plot(S.value*1.0E+3,zc*1.0E-3,'g-')
            axS.set_ylim([Lx*1.0E-3,0])
            axS.set_xlabel('Salinity [g/L]')
            axS.grid()

            axrho.plot(rhof.value,zf*1.0E-3,'c-')
            axrho.set_ylim([Lx*1.0E-3,0])
            axrho.set_xlabel('$\\rho_B$ [kg/m$^3$]')
            axrho.grid()

            axmu.plot(mu.value*1.0E+3,zf*1.0E-3,'m-')
            axmu.set_ylim([Lx*1.0E-3,0])
            axmu.set_xlabel('$\\mu_B$ [cP]')
            axmu.grid()

            plt.tight_layout()
            plt.suptitle('iter=%i t=%.3g yr' % (j,t/31556925.9747,))
            plt.savefig('density_flow_results_1D_%3.3i.png' % (j,))
            plt.close(1)            
            
    if not save_figs: 
        pviewer.plot()
        Tviewer.plot()
        Sviewer.plot()
