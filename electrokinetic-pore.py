from fipy import Grid2D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, ExponentialConvectionTerm, Viewer
##import matplotlib
##matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy 

save_plots = False

# 1D cartesian pore (slit)
# stokes flow is given by steady-state distribution (parabola)
# nernst-planck equation for transport of ions
# poisson equation for electrostatics

# slit pore with flow in x direction and
# and pore width is h in y direction

dx,dy = (1.0E-5, 1.0E-5)
nx,ny = (300, 50)
dt = 2.0E-6
nt = 300

close = min(dx,dy)/8.0

h = 2.0*(ny*dy) # pore width (only simulate top half -- y=0 is symmetry plane in middle)
L = nx*dx # pore length

zeta = 25.0E-3 # surface potential (V)
qpp = -1.0E-8  # surface charge density (compute from zeta?)
D = 1.0E-9  # diffusion coefficient (m^2/sec)
Umax = 5.0E-1 # max flow velocity (m/sec)

zp = +1.0  # ion valence for simple symmetric salt (+1/-1) 
zm = -1.0
z = zp   

e = 1.6021766E-19  # elementary charge on electron (Coulombs)
Na = 6.022141E+23  # Avagadro's number (1/mole)
F = e*Na  # Faraday constant (Coulombs/mole)
#print 'Faraday',F,'C/mole'

R = 8.31446  # gas constant (Volts*Coulombs/Kelvin/mole = Joule/Kelvin/mole)
T = 298.15 # universal temperature [25 degrees C] (Kelvin) 
kB = R/Na # Boltzmann constant (V*C/K = J/K)
#print 'Boltzmann',kB,'J/K'

epsilon0 = 8.854187817E-12 # Vacuum permittivity (Farads/meter)
epsilonr = 80.0 # relative permittivity of water
epsilon = epsilon0*epsilonr
#print 'permittivity water',epsilon,'F/m'

# far-stream concentration (assumed symmetric 1:1 or 2:2, etc.)
ninf = 1.0E-6

# debye length 
kappa = numpy.sqrt(2*F*e*z**2*ninf/(epsilon*R*T))
#print 'Debye length',kappa,'1/m'

#print 'pore length',L,'m'
#print 'pore width',h,'m'
#print 'scaled pore width',h*kappa,'[-]'

# characteristic potential
PsiC = R*T/(z*F)
print 'Psi_C',PsiC,'V'

m = Grid2D(dx=dx, nx=nx, dy=dy, ny=ny)

u = FaceVariable(name='$u$', mesh=m, value=0.0, rank=1)
xf,yf = m.faceCenters()  # for plotting face-centered fluxes
xc,yc = m.cellCenters()  # for plotting cell-centered potentials

# parabolic steady-state velocity profile in x-direction
# depending on y-coordinate (scaled to -1 <= y <= 1)

u.setValue([Umax*(1.0 - (2.0*yf/h)**2), numpy.zeros_like(xf)])

if save_plots:
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    # plot velocity profile along inflow boundary
    mask = m.facesLeft.value
    ax.plot(yf[mask], u[0,mask],'k-')
    ax.set_xlabel('$y$')
    ax.set_ylabel('$u_x$')
    ax.set_title('$x$-velocity profile')
    plt.savefig('debug_u.png')
    plt.close(1)

# electric potential
psi = CellVariable(name='$\\psi$', mesh=m, value=0.0)

# specified surface charge density along Stern plane
psi.faceGrad.constrain(-qpp/epsilon, m.facesTop)
###psi.faceGrad.constrain( qpp/epsilon, m.facesBottom)

psi.constrain(0.0, m.facesLeft)  # fixed potential in inflow
psi.faceGrad.constrain(0.0, m.facesRight)

# number concentration of ions (1/m^3})
np = CellVariable(name='$n_+$', mesh=m, value=ninf)
nm = CellVariable(name='$n_-$', mesh=m, value=ninf)

np.faceGrad.constrain(0.0, m.facesTop)
##np.faceGrad.constrain(0.0, m.facesBottom)
np.constrain(ninf, m.facesLeft)
np.constrain(ninf, m.facesRight)

nm.faceGrad.constrain(0.0, m.facesTop)
##nm.faceGrad.constrain(0.0, m.facesBottom)
nm.constrain(ninf, m.facesLeft)
nm.constrain(ninf, m.facesRight)

Poisson = DiffusionTerm(coeff=epsilon, var=psi) == -e*(zp*np + zm*nm)

NFNernstPlanckp = (DiffusionTerm(coeff=D, var=np) +
                 DiffusionTerm(coeff=e*D*zp/(kB*T)*np, var=psi))
    
NFNernstPlanckm = (DiffusionTerm(coeff=D, var=nm) +
                 DiffusionTerm(coeff=e*D*zm/(kB*T)*nm, var=psi))

NernstPlanckp = (TransientTerm(coeff=1.0, var=np)  ==
                 -ExponentialConvectionTerm(coeff=u, var=np)  +
                 DiffusionTerm(coeff=D, var=np) +
                 DiffusionTerm(coeff=e*D*zp/(kB*T)*np, var=psi))

NernstPlanckm = (TransientTerm(coeff=1.0, var=nm) ==
                 -ExponentialConvectionTerm(coeff=u, var=nm) +
                   DiffusionTerm(coeff=D, var=nm) +
                 DiffusionTerm(coeff=e*D*zm/(kB*T)*nm, var=psi))

NFEqns = Poisson & NFNernstPlanckp & NFNernstPlanckm
Eqns   = Poisson & NernstPlanckp   & NernstPlanckm

viewer = Viewer(vars=(psi,np,nm))

# solve no-flow version of equations
NFEqns.solve()
viewer.plot()

if save_plots:
    fig = plt.figure(7,figsize=(11,11))
    yprofile = yc[(xc - xc.min()) < close] # vectors of coordinates
    xprofile = xc[(yc - yc.min()) < close]

    ax = []
    for j in range(4):
        ax.append(fig.add_subplot(2,2,j+1))

    my = (yc - yprofile[0]) < close # bottom row
    mx = (xc - xprofile[0]) < close # left column
    
    ax[0].plot(xprofile, np[my],        'r-', label='$n_+ (y=0)$')
    ax[0].plot(xprofile, nm[my],        'g-', label='$n_- (y=0)$')
    ax[1].plot(xprofile, psi[my],       'b-', label='$y=0$') # psi
    ax[2].plot(xprofile, np[my]+nm[my], 'c-', label='$y=0$') # rho_f

    print 'np[mx]',np[mx]
    print 'nm[mx]',nm[mx]
    print 'psi[mx]',psi[mx]
    
    ax[3].plot(yprofile, np[mx],  'r-', label='$n_+ (x=0)$')
    ax[3].plot(yprofile, nm[mx],  'g-', label='$n_- (x=0)$')
    ax.append(ax[3].twinx())
    ax[4].plot(yprofile, psi[mx], 'b-', label='$x=0$')
    
    for j in range(3):
        ax[j].set_xlabel('$x$')
    ax[3].set_xlabel('$y$')

    ylabs = ['$n_{\\pm}$','$\\psi$','$\\rho_f$','$n_{\\pm}$','$\\psi$']

    for j in range(5):
        ax[j].set_ylabel(ylabs[j])

    my = (yc - yprofile[int(ny/2)]) < close # middle row
    mx = (xc - xprofile[int(nx/2)]) < close # middle column
    
    ax[0].plot(xc[my], np[my],        'r--', label='$n_+ (y=h/4)$')
    ax[0].plot(xc[my], nm[my],        'g--', label='$n_- (y=h/4)$')
    ax[1].plot(xc[my], psi[my],       'b--', label='$y=0$') # psi
    ax[2].plot(xc[my], np[my]+nm[my], 'c--', label='$y=h/4$') # rho_f
    
    ax[3].plot(yc[mx], np[mx],  'r--', label='$n_+ (x=L/2)$')
    ax[3].plot(yc[mx], nm[mx],  'g--', label='$n_- (x=L/2)$')
    ax[4].plot(yc[mx], psi[mx], 'b--', label='$x=L/2$')

    my = (yc - yprofile[ny-1]) < close  # top row
    mx = (xc - xprofile[nx-1]) < close  # right column

    ax[0].plot(xc[my], np[my],        'r:', label='$n_+ (y=h/2)$')
    ax[0].plot(xc[my], nm[my],        'g:', label='$n_- (y=h/2)$')
    ax[1].plot(xc[my], psi[my],       'b:', label='$y=h/2$') # psi
    ax[2].plot(xc[my], np[my]+nm[my], 'c:', label='$y=h/2$') # rho_f
    
    ax[3].plot(yc[mx], np[mx],  'r:', label='$n_+ (x=L)$')
    ax[3].plot(yc[mx], nm[mx],  'g:', label='$n_- (x=L)$')
    ax[4].plot(yc[mx], psi[mx], 'b:', label='$x=L$')

    for j in range(4):
        ax[j].legend(loc=0)
    plt.suptitle('no-flow electrokinetic solution')
    plt.tight_layout()
    plt.savefig('debug-no-flow.png')
    plt.close(7)
    
t = 0.0
# solve steady-state flow versions of equations
for j in xrange(nt):
    t += dt
         
    Eqns.solve(dt=dt)
    print j,t
    if j%5 == 0:
        viewer.plot()

