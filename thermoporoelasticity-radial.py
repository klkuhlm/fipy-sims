from fipy import CylindricalGrid1D, CellVariable, TransientTerm, DiffusionTerm, Viewer
import numpy as np

L = 2500.0  # analytic solution is for a half-space
nr = 2000
dr = L/nr
timeStep = 0.5 # dx/10.0
steps = 200

kappa = 1.0 # thermal diffusivity [m/s^2]
alpha = 1.0 # hydraulic diffusivity [m/s^2]
alphaD = kappa/alpha 

# radial flow example from McTigue (SAND85-1149)
m = CylindricalGrid1D(dx=dr, nx=nr, origin=(1,))

theta = CellVariable(name='$\\theta_D$', mesh=m, value=0.0)
p =     CellVariable(name='$p_D$', mesh=m, value=0.0)

# thermal BC
##theta.faceGrad.constrain(-1, m.facesLeft) # specified flux at x=0
theta.constrain(1.0, m.facesLeft) # specified temp =1 at x=0
##theta.faceGrad.constrain(0.0, m.facesRight) # specified no-flow at x=L
theta.constrain(0.0, m.facesRight)

# hydraulic BC
p.constrain(0.0, m.facesLeft) # drained at x=0
##p.faceGrad.constrain(0.0, m.facesLeft) # no-flow at x=0
p.faceGrad.constrain(0.0, m.facesRight)  # zero flux at x=L

# poroelasticity governing equation
eq1 = (TransientTerm(1.0,var=p) - DiffusionTerm(1.0,var=p) == TransientTerm(1.0,var=theta))
# heat conduction governing equation
eq2 = (TransientTerm(1.0/alphaD,var=theta) == DiffusionTerm(1.0,var=theta))

eq = eq1 & eq2 # coupled
viewer1 = Viewer(vars=(theta,p),datamin=0.0,datamax=1.0)
viewer1.plot()

t=0

for i,step in enumerate(np.logspace(-1,3.5,steps)):
    t += step
    eq.solve(dt=step)
    print t
    viewer1.plot(filename='soln_%5.5i.png'% (i,))

