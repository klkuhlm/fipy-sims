These python scripts use the fipy finite volume partial differential equation solver framework to solve a few problems of interest to me.

http://www.ctcms.nist.gov/fipy/

* Richards equation for 1D unsaturated porous media flow;
* thermohaline convection (fluid density dependent on salinity, temp & pressure) for 1D saturated porous medium;
* non-linear 2D & 3D heat conduction in salt around a heated excavation backfilled with crushed salt;
* thermoporoelasticity porous media flow to a borehole due to heating salt;
* electrokinetic (i.e., voltage) response of a layered aquifer to pumping; and
* double porosity porous media flow and double porosity solute transport.

These are mostly scripts I wrote to solve a specific problem/configuration or to better understand a concept I was working on.  These scripts are not really "finished products" for distribution, but may still be of use to others, or as examples of solutions using fipy.  NB: scripts might have bugs or untested parts.