from fipy import *
import matplotlib.pyplot as plt
import numpy as np

L = 10.0
nx = 100
dx = L/nx

phif = 0.01 
phim = 0.09 
gamma = 0.75 
beta = 0.05

#m = Grid1D(nx=nx, dx=dx)  # works with this grid
m = CylindricalGrid1D(nx=nx, dx=dx, origin=(0.1,)) # fails with this grid

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pf = CellVariable(name='$p_f$', mesh=m, value=0.0)
pm = CellVariable(name='$p_m$', mesh=m, value=0.0)

specFlux = +1.0E0
pf.faceGrad.constrain([specFlux,], m.facesLeft)
pf.faceGrad.constrain([0.0,], m.facesRight)

pm.faceGrad.constrain([0.0,], m.facesLeft) 
pm.faceGrad.constrain([0.0,], m.facesRight)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c0 = np.linspace(0.0,1.0,nx)
cf = CellVariable(name="$c_f$", mesh=m, value=c0)  
cm = CellVariable(name="$c_m$", mesh=m, value=1.0)

cf.faceGrad.constrain([specFlux,], m.facesLeft) # outflow BC
cf.faceGrad.constrain([0.0,], m.facesRight)

cm.faceGrad.constrain([0.0,], m.facesLeft)
cm.faceGrad.constrain([0.0,], m.facesRight)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PeqF = TransientTerm(0.05,var=pf) + TransientTerm(gamma,var=pm) == DiffusionTerm(1.0,var=pf)
PeqM = TransientTerm(1.0,var=pm) == (ImplicitSourceTerm(gamma,var=pf) - 
                                     ImplicitSourceTerm(gamma,var=pm))

CeqF = (TransientTerm(1.0,var=cf) + TransientTerm(beta,var=cm)  == 
        DiffusionTerm(1.0,var=cf) - ExponentialConvectionTerm(-pf.faceGrad/phif,var=cf))
CeqM = TransientTerm(phim,var=cm) == (ImplicitSourceTerm(beta,var=cf) - 
                                      ImplicitSourceTerm(beta,var=cm))
Peqns = PeqF & PeqM 
Ceqns = CeqF & CeqM

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fig = plt.figure(1,figsize=(11,7))
hax = fig.add_subplot(211)
cax = fig.add_subplot(212)

Pviewer = Viewer(vars=(pf,pm),datamax=0.0,datamin=-2.6,axes=hax,legend=4)
Pviewer.plot()
Cviewer = Viewer(vars=(cf,cm),datamax=1.0,datamin=-0.25,axes=cax,legend=4)
Cviewer.plot()

time = 0.0
split = 3.0E-2
dt = np.concatenate((np.logspace(-4,np.log10(split),50),np.ones((200,))*split),axis=1)

for timeStep in dt:
    time += timeStep
    print time

    Peqns.solve(dt=timeStep)
    Pviewer.plot()

    Ceqns.solve(dt=timeStep)
    Cviewer.plot()

