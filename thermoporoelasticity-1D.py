from fipy import Grid1D, CellVariable, TransientTerm, DiffusionTerm, Viewer
import numpy as np

L = 100.0  # analytic solution for a half-space
nr = 500
dr = L/nr
timeStep = 0.5 # dx/10.0
steps = 300

Ss = 1.0
kH = 1.0
kappaH = kH/Ss # hydraulic diffusivity
cp = 1.0  
kT = 1.0
kappaT = kT/cp # thermal diffusivity

c = 1.0 # equation 19 in McTigue solution
bprime = 1.0 # equation 28 in McTigue
qdivbyK = -1.0

# 1D examples from McTigue (SAND85-1149)

m = Grid1D(dx=dr, nx=nr)

theta = CellVariable(name='temperature',mesh=m, value=0.0)
p =     CellVariable(name='pressure',   mesh=m, value=0.0)

# case 1) constant temp BC on left
# thermal BC (specified temp at x=0) 
theta.constrain(1.0, m.facesLeft)

# case 2) constant flux BC on left
# thermal BC (specified flux at x=0) 
#theta.faceGrad.constrain(qdivbyK, m.facesLeft)

#theta.constrain(0.0, m.facesRight) # specified temp =0 at x=L
theta.faceGrad.constrain(0.0, m.facesRight)
# hydraulic (drained BC at x=0)
#p.constrain(0.0, m.facesLeft)

# hydraulic (no-flow BC at x=0)
p.faceGrad.constrain(0.0, m.facesLeft)
p.faceGrad.constrain(0.0, m.facesRight)  # zero flux at x=L

# thermoporoelasticity governing equation
eq1 = (TransientTerm(Ss,var=p) - DiffusionTerm(kH,var=p) == 
       TransientTerm(bprime,var=theta))

# heat conduction governing equation
eq2 = (TransientTerm(cp,var=theta) == DiffusionTerm(kT,var=theta))

# coupled
eq = eq1 & eq2

viewer1 = Viewer(vars=(theta,p))
viewer1.plot()

fh1 = open('solution-p.dat','w')
fh2 = open('solution-theta.dat','w')

t=0

for step in np.logspace(-1,2,steps):
    t += step
    eq.solve(dt=step)
   
    print t

    viewer1.plot()
    out = (t,p.getValue()[1],p.getValue()[10],
           p.getValue()[20],p.getValue()[-20])
    fh1.write('%.5e,%.5e,%.5e,%.5e,%.5e\n' % out)
    out = (t,theta.getValue()[1],theta.getValue()[10],
           theta.getValue()[20],theta.getValue()[-20])
    fh2.write('%.5e,%.5e,%.5e,%.5e,%.5e\n' % out)

fh1.close()
fh2.close()
