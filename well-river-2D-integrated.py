#import matplotlib
#matplotlib.use('Agg')

import fipy as fp
import numpy as np

dx = 1.0
dy = 1.0
nx = 100
ny = 100
steps = 1000
dt = 0.01

b = 1.0 # aquifer thickness (integrated away)

m = fp.Grid2D(dx=dx, nx=nx, dy=dy, ny=ny)
x,y = m.cellCenters()
X,Y = m.faceCenters()


# river is a strip parallel to y-axis
river_location = (x>13.0) & (x<16.0)
river_location_faces = (X>13.0) & (X<16.0)

# well is a single element, along x-axis (exploit symmetry)
well_location = (x>49.0) & (x<51.0) & (y<1.0)

# drawdown in aquifer 
s = fp.CellVariable(name="$s$", mesh=m, value=0.0)

# head in river (like secondary porosity)
# also need a "secondary permeability" that represents the river
# currently river cells are only connected through the aquifer
hr = fp.CellVariable(name="$h_r$", mesh=m, value=1.0)
#hr.setValue(1.0, where=river_location)

Ss = 1.0E-5 # aquifer specific storage
Kxy = 1.0E-2 # horizontal aquifer hydraulic conductivity

# well abstraction (source term at single location)
Q = fp.CellVariable(name="$Q$", mesh=m, value=0.0)
Q.setValue(-1.0E-3, where=well_location)

# stream storage coefficient (time derivative coefficient can't be zero)
Cr = fp.CellVariable(name='$C_r$', mesh=m, value=1.0E-3) 
#Cr.setValue(1.0E-3, where=river_location)

# beta = K'/b' (streambed conductance) (space derivative coefficient can be zero)
beta = fp.CellVariable(name="$\\beta$", mesh=m, value=0.0)
beta.setValue(1.0E-6, where=river_location)

aquifer = (fp.TransientTerm(Ss, var=s) - fp.TransientTerm(Cr, var=hr) == 
           fp.DiffusionTerm(Kxy, var=s) - Q/(b*np.pi*Kxy))

u = fp.FaceVariable(name="$u$", mesh=m, rank=1, value=((0.0,),(0.0,)))
u.setValue(((1.0,),(1.0,)), where=river_location_faces)

river = (fp.TransientTerm(Cr, var=hr) + fp.ExponentialConvectionTerm(coeff=u, var=hr)
         == -beta*(s - hr))

# couple equations
eqns = aquifer & river

viewer_s = fp.Viewer(vars=(s,)) # ,Q,beta,Cr
viewer_hr = fp.Viewer(vars=(hr,))
viewer_s.plot() #filename='s_initial.png')
viewer_hr.plot() #filename='hr_initial.png')
time = 0.0
#raw_input("enter to continue")

for step in range(steps):
    time += dt
    print step,time

    eqns.solve(dt=dt)
    #aquifer.solve(dt=dt)
    viewer_s.plot()#filename='s_%3.3i.png' % (step,))
    viewer_hr.plot()#filename='hr_%3.3i.png' % (step,))


