// based on t1.geo from gmsh tutorial

// mesh characteristic length
lc = 8.5E-5;

Point(1) = {0.0,    0.0,    0.0, lc};
Point(2) = {4.0E-3, 0.0,    0.0, lc};
Point(3) = {4.0E-3, 2.0E-3, 0.0, lc};
Point(4) = {3.5E-3, 2.0E-3, 0.0, lc};
Point(5) = {3.5E-3, 5.0E-4, 0.0, lc};
Point(6) = {5.0E-4, 5.0E-4, 0.0, lc};
Point(7) = {5.0E-4, 2.0E-3, 0.0, lc};
Point(8) = {0.0,    2.0E-3, 0.0, lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 1};

Line Loop(9) = {1, 2, 3, 4};
Line Loop(10) = {5, 6, 7, 8};
Plane Surface(11) = {9, 10};

//// define things with an id, that can be identified with BC
//LeftEnd = 90;
//RightEnd = 91;
//CenterLine = 92;
//NoSlipBdry = 93; // Everything else
//
//Physical Line(LeftEnd) = {8};
//Physical Line(RightEnd) = {2};
//Physical Line(CenterLine) = {1};
//Physical Line(NoSlipBdry) = {3, 4, 5, 6, 7};

