from fipy import *

L = 3.0 # radial length of domain (m)
b = 0.25 # thickness of a layer (m)
nr = 30
dr = L/nr

nz = 15
dz = b/nz

timeStep = 0.01 # dx/10.0
steps = 100

Ss = 2.0E-4 # 1/m
Kr = 4.5E-4 # m/s
Sy = 0.3
S = Ss*b
T = Kr*b

Q = 1.0E0

sigma = 1.0E-1 # S/m  
C = -13.00 # mV/m

# electrical problem is two layers (bottom one overlaps with flow problem
mesh = CylindricalGrid2D(dx=dr, nx=nr, ny=3*nz, dy=dz)

mesh.origin = array([[1.0E-4],
                       [0.0]])

s = CellVariable(name='drawdown',mesh=mesh, value=0.0, hasOld=True)
phi = CellVariable(name='voltage',mesh=mesh, value=0.0)

# specify flow properties of "aquitard" are very small (~no flow)
Tmat = FaceVariable(mesh=mesh, value=T)
z = mesh.getFaceCenters()[1]
Tmat.setValue(T/1000.0, where=(z > 2*b))
Tmat.setValue(T/1000.0, where=(z < b))

# make top layer of aquifer unconfined storage
Smat = FaceVariable(mesh=mesh, value=S)
#Smat.setValue(Sy, where=((z > 2*b - b/5.0) & (z < 2*b + 0.01)))

sigmamat = FaceVariable(mesh=mesh, value=sigma)
sigmamat.setValue(sigma/10, where=(z > 2*b))
sigmamat.setValue(sigma*10, where=(z < b))

# only pump from the middle layer
pumpingRate = Q/(2.0*pi*T)
aquiferWell = mesh.getFacesLeft() & (z > b) & (z < 2*b)

gwBCs = (FixedFlux(aquiferWell,pumpingRate),
         FixedValue(mesh.getFacesRight(),0.0))

elBCs =(FixedFlux(aquiferWell,-pumpingRate*C),
        FixedValue(mesh.getFacesRight(),0.0))

gw = TransientTerm(S) == DiffusionTerm(Tmat)
el = DiffusionTerm(sigmamat) == C*sigma*(s - s.getOld())/timeStep

viewer1 = Viewer(vars=(s,phi))
viewer1.plot()

time = 0.0

for step in range(steps):
    time += timeStep
    s.updateOld()

    print step,time

    gw.solve(s, dt=timeStep, boundaryConditions=gwBCs)
    el.solve(phi, dt=timeStep, boundaryConditions=elBCs)
    viewer1.plot()
