from fipy import *
import numpy as np

L = 10.0
nr = 100
dr = L/nr
timeStep = 0.05 # dx/10.0
steps = 300

Ss = 2.0E-4 # 1/m
Kr = 4.5E-4 # m/s
b = 16 # m
S = Ss*b
T = Kr*b
Sel = 1.0E-6

Q = 1.0E0

sigma = 4.0E-3 # S/m  
C = -5.00 # mV/m

mesh = CylindricalGrid1D(dx=dr, nx=nr)
mesh.origin = (1.0E-4,)

s = CellVariable(name='drawdown',mesh=mesh, value=0.0, hasOld=True)
phi = CellVariable(name='voltage',mesh=mesh, value=0.0)

well = Q/(2.0*np.pi*T)

gwBCs = (FixedFlux(mesh.getFacesLeft(),well),
       FixedValue(mesh.getFacesRight(),0.0))

elBCs =(FixedFlux(mesh.getFacesLeft(),-well*C),
         FixedValue(mesh.getFacesRight(),0.0))

gw = TransientTerm(S) == DiffusionTerm(T)
el = DiffusionTerm(sigma) == TransientTerm(Sel) + C*sigma*(s - s.getOld())/timeStep 

viewer1 = Viewer(vars=(s,phi))
#viewer2 = Viewer(vars=phi)
viewer1.plot()
#viewer2.plot()

for step in range(steps):
    s.updateOld()

    gw.solve(s, dt=timeStep, boundaryConditions=gwBCs)
    el.solve(phi, dt=timeStep, boundaryConditions=elBCs)
    viewer1.plot()
    #viewer2.plot()
