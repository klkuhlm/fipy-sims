from fipy import Grid1D, CellVariable, FaceVariable, TransientTerm, DiffusionTerm, ExponentialConvectionTerm, ImplicitSourceTerm, Viewer
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import quad

L = 10.0
nx = 100
dx = L/nx
timeStep = dx/20.0
steps = 150
outlocs = [0,49,69]

rt2 = np.sqrt(2.0)
rt2pi = np.sqrt(2.0*np.pi)

phif = 0.01   # fracture porosity
phim = 0.09  # matrix porosity

gamma = 0.9  # fracture-matrix head transfer rate
beta = 0.2  # fracture-matrix conc transfer rate

D = 1.0E-1    # fracture diffusion coeff
Rf = 1.0     # fracture retardation coefficient
Rm = 1.0     # matrix retardation coefficient

betaT = phim*Rm/(phif*Rf)
DR = D/Rf

Kf = 1.0       # fracture permeability
Ssf = 5.0E-2   # fracture specific storage

m = Grid1D(dx=dx, nx=nx)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# pseudo steady-state dual porosity flow solution (Warren & Root)
# analogous dual porosity transport solution 

# head/drawdown in fractures and matrix
hf = CellVariable(name='$h_f$',mesh=m, value=0.0)
hm = CellVariable(name='$h_m$',mesh=m, value=0.0)

# specified flux at one end/ constant head at other
hf.faceGrad.constrain(-1.0E0, m.facesLeft)
hf.constrain(0.0, m.facesRight)

# no flow condition for matrix at source
# no drawdown condition at other end
hm.faceGrad.constrain(0.0, m.facesLeft)
hm.constrain(0.0, m.facesRight)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# concentration in fractures and matrix

# non-zero initial concentration section of domain
c0 = np.zeros(nx, 'd')
c0[20:50] = 1.0

cf = CellVariable(name="$c_f$", mesh=m, value=c0)
cf.constrain(0, m.facesLeft)
cf.constrain(0, m.facesRight)

cm = CellVariable(name="$c_{m}$", mesh=m, value=0.0, hasOld=True)
cm.constrain(0, m.facesLeft)
cm.constrain(0, m.facesRight)

# mean and stdev of log(omega_D)
mu = 1.0
sigma = 2.0

# lognormal function
ln = lambda x : np.exp(-((np.log(x) - mu)/(sigma*rt2))**2)/(x*sigma*rt2pi)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Fracture flow equation
HeqF = TransientTerm(Ssf,var=hf) + TransientTerm(0.25,var=hm) == DiffusionTerm(Kf,var=hf)

# matrix flow equation
HeqM = TransientTerm(1.0,var=hm) == gamma*(ImplicitSourceTerm(1.0,var=hf) - 
                                           ImplicitSourceTerm(1.0,var=hm))

# convection diffusion equation for fractures 
CeqF =  (TransientTerm(1.0,var=cf) + quad(lambda x: ln(x)*x*(cm - cm.old())/timeStep,0,100) == 
         DiffusionTerm(DR,var=cf) - ExponentialConvectionTerm(-hm.faceGrad*(Kf/(Rf*phif)),var=cf))

# lumped approach for matrix
CeqM = TransientTerm(Rm*phim,var=cm[0]) == beta/Rm*(ImplicitSourceTerm(1.0,var=cf) - 
                                                    ImplicitSourceTerm(1.0,var=cm))

# couple pairs of equations
Heqns = HeqF & HeqM 
Ceqns = CeqF & CeqM

Hviewer = Viewer(vars=(hf,hm))
Hviewer.plot()
Cviewer = Viewer(vars=(cf,cm))
Cviewer.plot()

time = 0.0
dt = np.concatenate((np.logspace(-5,-2,50),np.ones((150,))*0.01),axis=1)

fh = open('solution-double-porosity-flow-and-transport.dat','w')
fmtstr = ','.join('%.5f' for i in range(len(outlocs)*4+1)) + '\n'

for timeStep in dt:
    time += timeStep
    print time

    Heqns.solve(dt=timeStep)
    Hviewer.plot()

    out = [time,]
    out.extend([hf.value()[i] for i in outlocs])
    out.extend([hm.value()[i] for i in outlocs])

    cm.updateOld()
    Ceqns.solve(dt=timeStep)
    Cviewer.plot()

    out.extend([cf.value()[i] for i in outlocs])
    out.extend([cm.value()[i] for i in outlocs])
    fh.write(fmtstr % tuple(out))
