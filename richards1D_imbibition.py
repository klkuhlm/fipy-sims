import fipy as fp
import numpy as np
from h5py import File
import vanGenuchten as vG

fn_out = "imbibition_vg"
print_to_screen = True
DEBUG = True

# initialize class
mrc = vG.vanGenuchten(m=0.5, alpha=1.0E-5)

def psat(T_K):
    # saturated vapor pressure
    # eqn 2.17 in Rogers & Yau (Cloud Physics)
    # T is passed in as K
    # equation is for mbar/hPa -> *100
    T_C = T_K - 273.15
    return 6.112 * np.exp(17.67 * T_C / (T_C + 243.5)) * 100.0


def rh(Td_K,Tw_K):
    # compute relative humidity from dry and wet bulb temperatures
    Td_C = Td_K - 273.15
    Tw_C = Tw_K - 273.15
    N = 0.668745
    ed = psat(Td_K) / 100.0
    ew = psat(Tw_K) / 100.0
    return (ew - N * (1.0 + 0.00225 * Tw_C) * (Td_C - Tw_C)) / ed * 100.0 

# time in seconds
SECINHR = 60.0 * 60.0
SECINDAY = SECINHR * 24.0

# length
L = 0.15  # [m]
nx = 20
dx = L / nx  # [m]

# since this is sort of like an explicit convection (of moisture by gravity)
# solution (i.e., .divergence() term in PDE), timeStep must be kept small for large K
mesh = fp.Grid1D(dx=dx, nx=nx)
x_cc = mesh.cellCenters.value[0]  # [m]
x_fc = mesh.faceCenters.value[0]  # [m]

fh5 = File(fn_out + ".h5", "w")
fh5.create_dataset("Coordinates/X [m]", data=np.array([0.0,0.5]))
fh5.create_dataset("Coordinates/Y [m]", data=np.array([0.0,0.5]))
fh5.create_dataset("Coordinates/Z [m]", data=x_fc)
prov = fh5.create_group("Provenance") # Paraview expects this?
# For documenting inputs associated with outputs
param = fh5.create_group("Parameters") 

# saturated/common properties
# could make this heterogeneous by making it a FaceVariable
ks = 1.0e-14  # saturated permeability [m^2]
param.attrs["Permeability_[m^2]"] = ks

por = 0.32  # porosity
param.attrs["Porosity_[-]"] = por

rho = 997.0  # [kg/m^3] liquid water density
param.attrs["Water_Density_[kg|m^3]"] = rho

theta_r = 0.05
param.attrs["Residual_saturation_[-]"] = theta_r

# "background"
pc0 = 1.0E+7
param.attrs["Background_Capillary_Pressure_[m]"] = pc0
pc = fp.CellVariable(name="$p_c$", mesh=mesh, value=pc0, hasOld=True)  # [Pa] capillary pressure


cw = fp.CellVariable(name="$c_w$",mesh=mesh, value=)

cv = fp.CellVariable(name="$c_v$", mesh=mesh, value=0.0, hasOld=True)  # [kg/m^3] vapor conc
cs = fp.CellVariable(name="$c_s$", mesh=mesh, value=0.0, hasOld=True)  # [kg/m^3] sorbed conc

T0 = 25.0 + 273.15
param.attrs["Initial_Temperature_[K]"] = T0
T = fp.CellVariable(name="$T$", mesh=mesh, value=T0, hasOld=True)  # [K] temperature
#T.constrain(T0, where=mesh.facesLeft) # hold water table at constant temperature

Psat = fp.CellVariable(name="P_{sat}", mesh=mesh, value=0)  # [Pa] sat. vapor pressure

# to specify initial condition in terms of saturation, need h(Se) implemented for each model

C = fp.CellVariable(name="$C(h)$", mesh=mesh, value=0.0)  # moisture capacity
k = fp.FaceVariable(name="$k(S_e)$", mesh=mesh, value=0.0)  # hydraulic conductivity
Se = fp.FaceVariable(name="$S_e(h)$", mesh=mesh, value=0.0)  # saturation
Se_cell = fp.CellVariable(name="$S_e(h)$", mesh=mesh, value=0.0)  # saturation cell variable

D = 2.49e-5  # [m^2/sec] diffusion of vapor in air
param.attrs["Vapor_Diffusion_in_Air_[m|s]"] = D
tort = 1.0E-3
param.attrs["Tortuosity_[-]"] = tort
Drel = fp.FaceVariable(name="$D_{eff}$", mesh=mesh, value=D * tort * por)

pcBC = 100.0
param.attrs["Boundary_Capillary_Pressure_[Pa]"] = pcBC
pc.constrain(pcBC, mesh.facesLeft)  # water table condition at bottom

cp = 4186.0  # [J/kg*K] water specific heat

param.attrs["Specific_Heat_of_Bulk_[J|kg*K]"] = cp

# eventually should use some sort of bulk rho*cp that accounts for solid + saturating fluid

mu = 1.0E-3 # [Pa*s] water viscosity (eventually a function of temperature)
param.attrs["Water_Viscosity_[Pa*s]"] = mu

pcv = pc.value

C.setValue(mrc.calc_C(pcv))
Se.setValue(mrc.calc_Se(pc.harmonicFaceValue))
Se_cell.setValue(mrc.calc_Se(pcv))
k.setValue(mrc.calc_Kr(Se.value) * ks)

# 1) Richards' equation
g = 9.81 # m/s^2
gvec = [g]  # gravity "vector"
Req = (fp.TransientTerm(coeff=C, var=pc) ==
       fp.DiffusionTerm(coeff=k/mu, var=pc) + (k/mu * rho * g).divergence)

# dimensionless inter-porosity exchange coefficients
xis = 1.0e-1
xik = 10.0
param.attrs["Inter-Porosity_Storatge_Sorption_[-]"] = xis
param.attrs["Inter-Porosity_Storage_Kelvin_[-]"] = xik

# 2) primary porosity (with spatial diffusion)
Drel.setValue(D * (1.0 - Se) ** 3 * por * tort)
eqV = (fp.TransientTerm(1.0, var=cv) +
       fp.TransientTerm(xik, var=ck) + fp.TransientTerm(xis, var=cs) ==
       fp.DiffusionTerm(Drel, var=cv))

epsk = 1.0e-3  # [m^2] analogous to Warren-Root alpha
epss = 1.0e-4
param.attrs["Inter-Porosity_Permeability_Sorption_[-]"] = epss
param.attrs["Inter-Porosity_Permeability_Kelvin_[-]"] = epsk

# secondary porosity (connect with Kelvin term)
eqK = fp.TransientTerm(1.0, var=ck) ==  D / epsk * cv - fp.ImplicitSourceTerm(D / epsk, var=ck)

# tertiary porosity (connect with sorbed)
eqS = fp.TransientTerm(1.0, var=cs) == D / epss * cv - fp.ImplicitSourceTerm(D / epss, var=cs) 

# 3) thermal convection
# could make kT and cp function of saturation
Lv = 2256.0e3 / 1000.0  # [J/kg] specific latent heat of vaporization of water
Lhw = 8800.0 / 1000.0 # [J/kg] latent heat of wetting (from Good et al., 2023)

param.attrs["Specific_Latent_Heat_of_Vaporiation_of_Water_[J|kg]"] = L
param.attrs["Specific_Latent_Heat_of_Wetting_[J|kg]"] = Lhw
           
Twetbulb = 10.0 + 273.15  # [K] wet bulb temperature around sample
print(f"dry bulb: {T0:.2f} K, wet bulb: {Twetbulb:.2f} K, RH: {rh(T0,Twetbulb):.1f}%")

hBC = 4.8  # [W/m^3*K] value for a free surface (not porous)

param.attrs["Wet_Bulb_BC_Temp_[K]"] = Twetbulb
param.attrs["Wet_Bulb_BC_Newton_Coeff_[W|m^3*K]"] = hBC

hevap = fp.CellVariable(name="$h_{evap}$", mesh=mesh, value=0.0)
hevap.setValue(hBC * Se_cell**3 * por)
kT = 2.0  # [W/m*K] thermal conductivity (eventually a function of liquid saturation)

param.attrs["Thermal_Conductivity_[W|m*K]"] = kT

eqT = (
    fp.TransientTerm(rho * cp, var=T)
    + fp.ExponentialConvectionTerm(rho * cp * k/mu * (pc.faceGrad + rho * g), var=T)
    == fp.DiffusionTerm(kT, var=T) 
    + fp.TransientTerm(L, var=cv) # these source terms too strong?
    + fp.TransientTerm(Lhw, var=cs)
    + fp.ImplicitSourceTerm(hevap, var=T)
    - hevap * Twetbulb
)

# fully coupled vapor and temperature
eqVapThermal = eqV & eqK & eqS & eqT

sigma = 0.073  # N/m water-silica surface tension
Rsp = 461.5  # J/(kg*K) specific gas constant for water vapor

param.attrs["Water_Silica_Surface_Tension_[N|m]"] = sigma

t = 0.0  # initial time (sec)
dt = 0.01
dt_min = 1.0e-5
max_num_time_steps = int(1.0e6)
max_time = 20.0 * 60.0 # 20 min
delta_t_save = 1.0 
dt_max = 0.1 * delta_t_save
num_saves = int(max_time / delta_t_save)
save_times = (delta_t_save * (np.arange(num_saves, dtype=float))).tolist()
save_times.append(None)
save_next_dt = True  # save t0 fig/data on first step
prev_dt = dt
save = False
converged = True
max_iter = 20
iter_rel_tol = 1.0e-3
iter_tol = 1.0e-3
outer_idx = 0
save_idx = 0
    
# write timesteps / iterations to screen and .out file
fh = open(fn_out + ".out", "w")
fh.write("time_step,dt_[s],t_[s],R_iter,R_residual,vT_iter,vT_residual\n")

while outer_idx <= max_num_time_steps and t < max_time:

    if converged:
        pc.updateOld()

    Rres = 100.0
    R_idx = 0
    res_init = Rres

    # step 1: solve non-linear Richards equation

    while (Rres/res_init > iter_rel_tol and Rres > iter_tol) and R_idx < max_iter:

        # update mrc within non-linear iteration
        C.setValue(mrc.calc_C(pc.value))
        Se.setValue(mrc.calc_Se(pc.harmonicFaceValue))
        k.setValue(mrc.calc_Kr(Se.value) * ks)

        Rres = Req.sweep(dt=dt, var=pc)
        if R_idx == 0:
            res_init = Rres
            
        R_idx += 1
        if DEBUG:
            print("R",Rres,Rres/res_init,R_idx)

    if R_idx >= max_iter:
        print("**WARNING: non-linear Richards Picard iteration did not converge**")
        print(R_idx, Rres)
        
    # step 2: solve coupled thermal/vapor equations

    C.setValue(mrc.calc_C(pc))
    Se.setValue(mrc.calc_Se(pc.harmonicFaceValue))
    k.setValue(mrc.calc_Kr(Se) * ks)

    Psat.setValue(psat(T))

    # compute vapor concentration in kelvin domain
    ck.setValue(Psat.value / (Rsp * T.value) * np.exp(-pc.value / (rho * Rsp * T.value)))
    
    # update thermal/transport properties that depend on liquid saturation
    Drel.setValue(D * (1.0 - Se.value) ** 3 * por * tort)
    Se_cell.setValue(mrc.calc_Se(pc.value))
    hevap.setValue(hBC * Se_cell.value**3 * por)

    if DEBUG:
        print("Pc",pc.value)
        print("Psat",Psat.value)
        print("T",T.value)
        print("cv",cv.value)
        print("ck",ck.value)
        print("cs",cs.value)
        print("Se",Se.value)
        print("Se_cell",Se_cell.value)
        print("D(Se)",Drel.value)
        print("h_evap",hevap.value)
    
    if converged:
        cv.updateOld()
        ck.updateOld()
        cs.updateOld()
        T.updateOld()
    
    vres = 100.0
    res_init = vres
    v_idx = 0

    while (vres/res_init > iter_rel_tol and vres > iter_tol) and v_idx < max_iter:

        vres = eqVapThermal.sweep(dt=dt)
        if v_idx == 0:
            res_init = vres
            
        v_idx += 1
        if DEBUG:
            print("cv/T",vres,vres/res_init,v_idx)

    if v_idx >= max_iter:
        print("**WARNING: non-linear vapor/thermal Picard iteration did not converge**")
        print(v_idx, vres)
            
    inner_idx = max(v_idx,R_idx)
    res = max(vres,Rres)

    if save_times[0] == None:
        break

    else:
        # check convergence, update dt
        if inner_idx < max_iter / 3:
            # converged in only a few nonlinear iterations
            # take a larger time step next time
            dt = min(dt * 1.08, dt_max)
            converged = True
            outer_idx += 1

        elif inner_idx >= max_iter and res/res_init > iter_rel_tol:
            # didn't converge in max iterations,
            # reduce timestep and try again
            dt = max(dt * 0.5, dt_min)
            converged = False

        elif inner_idx > 2 * max_iter / 3:
            # converged, but taking quite a few iterations
            # take a slightly smaller time step next time
            dt = max(dt * 0.99, dt_min)
            converged = True
            outer_idx += 1

        else:
            # converged, slight increase in timestep
            converged = True
            dt = min(dt * 1.015, dt_max)
            outer_idx += 1

        # handle plotting figure this/next time step
        if converged and t + dt >= save_times[0]:
            if save_next_dt:
                dt = prev_dt
                del save_times[0]
                save_next_dt = False
                save = True
            else:
                prev_dt = dt
                if save_times[0] != None:
                    dt = save_times[0] - t
                    save_next_dt = True

    # write timestep data to text file
    fh.write(f"{outer_idx},{dt:.7g},{t:.7g},{R_idx},{Rres:.7g},{v_idx},{vres:.7g},{converged}\n")

    # print convergence/progress to screen
    if print_to_screen:
        print(
            f"{outer_idx:04d}, ( {R_idx:02d} {Rres:.3e} ) ( {v_idx:02d} {vres:.3e} ) dt={dt:.3e} s"
            f" t={t/60.0:.3e} min, {converged}"
        )

    if DEBUG:
        save = True
    
    if save:
        save = False
        if print_to_screen:
            print(f"saving output, {save_idx}")

        # write arrays of output into h5 files using PFLOTRAN-compatible format
        # fipy matrix format is flattened form of [z,x], while paraview expects 3D matrix [x,y,z]
        ts = f"Time:  {t:.5e} s"
        fh5.create_dataset(
            f"{ts}/Capillary_Pressure_[Pa]", data=pc.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Vapor_Concentration_[kg|m^3]", data=cv.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Saturated_Vapor_Pressure_[Pa]", data=Psat.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Vapor_Kelvin_[kg|m^3]", data=ck.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Vapor_Sorbed_[kg|m^3]", data=cs.value[None, None, :]
        )
        fh5.create_dataset(f"{ts}/Temperature_[K]", data=T.value[None, None, :])
        fh5.create_dataset(
            f"{ts}/Moisture_Capacity_[-]", data=C.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Relative_Permeability_[m^2]", data=k.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Relative_Liquid_Saturation_[-]", data=Se.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Relative_Gas_Diffusion_[m^2|s]", data=Drel.value[None, None, :]
        )
        fh5.create_dataset(
            f"{ts}/Relative_Newton_BC_[W|m^3 K]", data=hevap.value[None, None, :]
        )
        save_idx += 1
        
    if converged:
        # update time ready for next timestep
        t += dt

fh.close()
fh5.close()
print("finished.")
