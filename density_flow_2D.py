import fipy as fp
import numpy as np
import batzle_wang as eos
#import voss_souza as eos
from h5py import File
    
print_to_screen = True
isothermal = False

fn_out = 'elder_results_bw'

# time in seconds
SECINHR = 60.0*60.0
SECINDAY = SECINHR*24.0
SECINYR = SECINDAY*365.25

# length in meters
Lx,Lz = 600.0,150.0 
nx,nz = 35,20
dx = Lx/nx
dz = Lz/nz

m = fp.Grid2D(dx=dx, dy=dz, nx=nx, ny=nz)
Xc,Zc = m.cellCenters.value # for cell-centered variables
Xf,Zf = m.faceCenters.value # for face-centered fluxes

Tinit = 25.0
T = fp.CellVariable(name='$T$', mesh=m, value=Tinit, hasOld=True)
p = fp.CellVariable(name='$p$', mesh=m, value=0.0, hasOld=True)
S = fp.CellVariable(name='$X_{NaCl}$', mesh=m, value=0.0, hasOld=True)
#S.setValue(1.0E-5 + fp.GammaNoiseVariable(mesh=m, shape=1.0E-4, rate=1.0E-6))
S0 = 1.0E-8
S.setValue(S0)  # EOS doesn't like S=0

# constant coefficients
# ========================================
g = 9.81 # m/s^2
patm = 1.01325E+5 # 1 atm in Pa 
# mass diffusivity (molecular dispersion)
Dm = 3.565E-6  # m^2/sec

# initial conditions
# ========================================

# S has units of NaCl mass fraction [ppm*1.0E-6]
SBC = 0.266 # 1200 kg/m^2 at patm & 25 C

# density and viscosity used in flux/diffusion calcs
# therefore need face-centered version
p.setValue((Lz - Zc)*997.1*g + patm)  # [Pa] constant 25C fresh density (initial guess)

rhob0 = eos.brine_density(T.value, p.value, S.value)
    
# face variables that depend on system state
# ========================================
rho_f = fp.FaceVariable(mesh=m)
rho_f.setValue(eos.brine_density(T.harmonicFaceValue.value,
                                p.harmonicFaceValue.value,
                                S.harmonicFaceValue.value))

rho_fc = fp.CellVariable(mesh=m)
rho_fc.setValue(eos.brine_density(T.value, p.value, S.value))

mu = fp.FaceVariable(mesh=m)
mu.setValue(eos.viscosity(T.harmonicFaceValue.value,
                         S.harmonicFaceValue.value))

muc = fp.CellVariable(mesh=m)  # only for plotting/saving 

# cell variables that depend on system state
# ========================================
drhodp = fp.CellVariable(mesh=m) # = beta*rhof (compressibility*density)
drhodp.setValue(eos.drhob_dp(T.value, p.value, S.value))

drhodT = fp.CellVariable(mesh=m)
drhodT.setValue(eos.drhob_dT(T.value, p.value, S.value))

drhodS = fp.CellVariable(mesh=m)
drhodS.setValue(eos.drhob_dS(T.value, p.value, S.value))

# parameters that may be variable in space
# ========================================

# [m^2] intrinsic permeability
k = 4.845E-13
#k = fp.FaceVariable(mesh=m, value=1.0E-12)

# porosity changes due to compressibility (?)
phi = 0.1
#phi = fp.CellVariable(mesh=m, value=phi0)
alpha = 1.0E-10 # bulk aquifer compressibility [1/Pa] (Zheng & Bennett, p 537)

if not isothermal:
    
    # [W/(m*K)] bulk (wet) thermal conductivity (generally a decreasing f(T))
    # Eppelbaum et al. Applied Geothermics (2014) Table 2.3
    kT = fp.FaceVariable(mesh=m, value=1.0/(0.3514 + 3.795E-4*T.harmonicFaceValue))

    # [J/(m^3*K)] bulk heat capacity (density * specific heat)
    c_g = 790.0 # [J/(kg*K)] granite specific heat
    rho_g = 2750.0 # [kg/m^3] granite density

    # water specific heat = 4.186 joule/(gram*K)
    c_f = 4186.0 # J/(kg*K) water specific heat (not a strong f(T))

    # bulk heat capacity [J/(m^3*K)] (mixture of solid and liquid)
    rhoc_b = fp.CellVariable(mesh=m, value=((1.0 - phi)*rho_g*c_g + phi*rho_fc*c_f))

    # denser fluid at top is also slightly cooler
    cool_top = fp.Constraint(Tinit-5.0, where=(m.facesTop & ((Xf > 150.0) & (Xf < 450.0))))
    constant_bottom = fp.Constraint(Tinit, where=m.facesBottom)
    
    T.constrain(cool_top)
    T.constrain(constant_bottom)
    TBCon = True

S.constrain(SBC, where=(m.facesTop & ((Xf > 150.0) & (Xf < 450.0))))
S.constrain(S0, where=m.facesBottom)

p.constrain(patm, where=(m.facesLeft  & (Zf > Lz-dz))) # specified pressure in "corner"
p.constrain(patm, where=(m.facesRight & (Zf > Lz-dz)))

if not isothermal:
    # ensure water flowing into domain from constant pressure elements is at Tinit
    T.constrain(Tinit, where=(m.facesLeft  & (Zf > Lz-dz)))
    T.constrain(Tinit, where=(m.facesRight & (Zf > Lz-dz)))

# governing equations
# ========================================

gvec = [0.0,g]  # gravity acting down (-z), this is g*unit vector
gsave = np.zeros((2,p.value.shape[0]))
gsave[1,:] = g

ConvectionTerm = fp.PowerLawConvectionTerm 

# no piezometric pressure gradient (convection) across top and bottom boundaries
# scr{P} = p + rho*g*z (z is positive up)
p.faceGrad.constrain(-rho_f*gvec, m.facesTop)
p.faceGrad.constrain(-rho_f*gvec, m.facesBottom)

# transient single-porosity porous media flow
peq = (fp.TransientTerm(coeff=(phi*drhodp + rho_fc*alpha), var=p) +
       fp.TransientTerm(coeff=phi*drhodS, var=S) == 
       fp.DiffusionTerm(coeff=rho_f*k/mu, var=p) +
       (rho_f**2*gvec*k/mu).divergence)

if not isothermal:
    peq += fp.TransientTerm(coeff=phi*drhodT, var=T)
    
    # transient thermal convection
    Teq = (fp.TransientTerm(coeff=(rhoc_b + phi*c_f*drhodT), var=T) +
           fp.TransientTerm(coeff=phi*c_f*drhodp, var=p) +
           fp.TransientTerm(coeff=phi*c_f*drhodS, var=S) ==
           fp.DiffusionTerm(coeff=kT, var=T) +
           ConvectionTerm(coeff=rho_f*c_f*k/mu*(p.faceGrad + rho_f*gvec), var=T))

# transient soulte convection 
Seq = (fp.TransientTerm(coeff=phi, var=S) ==
       ConvectionTerm(coeff=k/mu*(p.faceGrad + rho_f*gvec), var=S) +
       fp.DiffusionTerm(coeff=Dm*phi, var=S))

if isothermal:
    eqns = peq & Seq 
else:
    eqns = peq & Seq & Teq

# ========================================
t = 0.0  # initial time (sec)
dt = SECINHR  # initial time step (sec)
dt_min = 1.0E-3
max_num_time_steps = int(1.0E+6)
max_time = 1.0*12.0*2.698E+6  # 20 years
delta_t_save = 2.698E+6 # 1 month (from Voss & Souza 1987)
dt_max =   0.1*delta_t_save 
num_saves = int(max_time/delta_t_save)
save_times = (delta_t_save*(np.arange(num_saves,dtype=float))).tolist()
save_times.append(None)
save_next_dt = True # save t0 fig/data on first step
prev_dt = dt
save = False
converged = True
max_iter = 35
iter_tol = 1.0E-2
j = 0

Xf_out = np.empty((nx+1,))
Xf_out[:-1] = Xc.reshape(nz,nx)[0,:] - dx/2.0
Xf_out[-1] = Lx

Zf_out = np.empty((nz+1,))
Zf_out[:-1] = Zc.reshape(nz,nx)[:,0] - dz/2.0
Zf_out[-1] = Lz

fh5 = File(fn_out + '.h5','w')
fh5.create_dataset('Coordinates/X [m]', data=Xf_out)
fh5.create_dataset('Coordinates/Y [m]', data=np.array([0.0,1.0])) # unit 
fh5.create_dataset('Coordinates/Z [m]', data=Zf_out)

prov = fh5.create_group('Provenance')

param = fh5.create_group('Parameters')
param.attrs['Permeability_[m^2]'] = k
param.attrs['Mass_Diffusivity_[m^2|s]'] = Dm
param.attrs['Porosity'] = phi
param.attrs['Soil_Compressibility_[1|Pa]'] = alpha
if not isothermal:
    param.attrs['Soil_Specific_Heat_[J|kg-K]'] = c_g
    param.attrs['Soil_Density_[kg|m^3]'] = rho_g
    param.attrs['Water_Specific_heat_[J|kg-K]'] = c_f

# implement zonation-based properties using mat_id (?)
mat_id = np.ones((p.value.shape[0],),dtype=int) 
    
# for logging output at each timestep
fh = open(fn_out + '.out','w')
fh.write('time_step,dt_[s],t_[s],nl_iter,residual\n')
    
while j <= max_num_time_steps and t < max_time:

    #if TBCon and t > 50.0*SECINDAY:
    #    # turn off heating at bottom edge at 20 days
    #    T.release(constraint=heat_bottom)
    #    TBCon = False

    if converged:
        p.updateOld()
        S.updateOld()
        T.updateOld()
        
    # perform non-linear fixed-point iteration
    # ==================================================
    res = 1.0E+5
    i = 0
    
    while res > iter_tol and i < max_iter:
        res = eqns.sweep(dt=dt) #,solver=solver)
        i += 1

        rho_f.setValue(eos.brine_density(T.harmonicFaceValue.value,
                                        p.harmonicFaceValue.value,
                                        S.harmonicFaceValue.value))
                
        mu.setValue(eos.viscosity(T.harmonicFaceValue.value,
                                 S.harmonicFaceValue.value))

        rho_fc.setValue(eos.brine_density(T.value, p.value, S.value))
        
        if not isothermal:
            rhoc_b.setValue((1.0 - phi)*rho_g*c_g + phi*rho_fc*c_f)
            
        # update derivatives of EOS used in storage terms
        drhodp.setValue(eos.drhob_dp(T.value, p.value, S.value))
        drhodT.setValue(eos.drhob_dT(T.value, p.value, S.value))
        drhodS.setValue(eos.drhob_dS(T.value, p.value, S.value))

    if save_times[0] == None:
        break # exit while loop
    
    else:
        
        # check convergence, update dt
        if i < max_iter/3:
            # converged in only a few nonlinear iterations
            # take a larger time step next time
            dt = min(dt*1.08, dt_max)
            converged = True
            j += 1

        elif i >= max_iter and res > iter_tol:
            # didn't converge in max iterations,
            # reduce timestep and try again
            dt = max(dt*0.5, dt_min)
            converged = False
            
        elif i > 2*max_iter/3:
            # converged, but taking quite a few iterations
            # take a slightly smaller time step next time
            dt = max(dt*0.99, dt_min)
            converged = True
            j += 1

        else:
            # converged, slight increase in timestep
            converged = True
            dt = min(dt*1.015, dt_max)
            j += 1

        # handle plotting figure this/next time step
        if converged and t + dt >= save_times[0]:
            if save_next_dt:
                dt = prev_dt
                del save_times[0]
                save_next_dt = False
                save = True
            else:
                prev_dt = dt
                if save_times[0] != None:
                    dt = save_times[0] - t
                    save_next_dt = True

    # write timestep data to text file
    fh.write('%i,%.7g,%.7g,%i,%.7g,%i\n' % (j,dt,t,i,res,converged))

    # print convergence/progress to screen
    if print_to_screen:
        print(j,'( %i %.3g ) dt=%.3g d' % (i,res,dt/SECINDAY,),end="")
        print(' t=%.5g d' % (t/SECINDAY,),converged)

    if save:
        save = False
        if print_to_screen:
            print('saving output')

        muc.setValue(eos.viscosity(T.value, S.value))

        # write arrays of output into h5 files using PFLOTRAN-compatible format
        # fipy matrix format is flattened form of [z,x], while paraview expects 3D matrix [x,y,z]
        time = 'Time:  %.5e d' % (t/SECINDAY,)
        fh5.create_dataset(time + '/Pressure_[Pa]',
                           data=p.value.reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/Pressure_Grad_X_[Pa|m]',
                           data=p.grad.value[0,:].reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/Pressure_Grad_Z_[Pa|m]',
                           data=p.grad.value[1,:].reshape(nz,nx).T[:,None,:])

        darcy = (p.grad.value +
                 rho_fc.value[None,:]*gsave)*k/(muc.value[None,:])
        fh5.create_dataset(time + '/Darcy_Flux_X_[m|s]',
                           data=darcy[0,:].reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/Darcy_Flux_Z_[m|s]',
                           data=darcy[1,:].reshape(nz,nx).T[:,None,:])


        fh5.create_dataset(time + '/Salinity_[X_NaCl]',
                           data=S.value.reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/Density_[kg|m^3]',
                           data=rho_fc.value.reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/Viscosity_[Pa-s]',
                           data=muc.value.reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/Material_ID',
                           data=mat_id.reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/drho|dp_[kg|m^3-Pa]',
                           data=drhodp.value.reshape(nz,nx).T[:,None,:])
        fh5.create_dataset(time + '/drho|dS_[kg|m^3-X_NaCl]',
                           data=drhodS.value.reshape(nz,nx).T[:,None,:])

        if not isothermal:
            fh5.create_dataset(time + '/Thermal_Conductivity_[W|m-K]',
                               data=1.0/(0.3514 + 3.795E-4*T.value.reshape(nz,nx).T[:,None,:]))
            fh5.create_dataset(time + '/Temperature_[C]',
                               data=T.value.reshape(nz,nx).T[:,None,:])
            fh5.create_dataset(time + '/drho|dT_[kg|m^3-K]',
                               data=drhodT.value.reshape(nz,nx).T[:,None,:])

        
    if converged:
        # update time ready for next timestep
        t += dt

# done
fh.close()
fh5.close()
print('finished.')

