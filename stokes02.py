from fipy import Grid2D, CellVariable, DiffusionTerm, TransientTerm, Viewer
from fipy.variables.faceGradVariable import _FaceGradVariable

L = 1.0
N = 50
dl = L/N
viscosity = 1.0
lid = 1.0
rho = 1.0

mesh = Grid2D(nx=N, ny=N, dx=dl, dy=dl)

p = CellVariable(mesh=mesh, name='p', value=0.0, hasOld=True)
u = CellVariable(mesh=mesh, name='Ux', value=0.0, hasOld=True)
v = CellVariable(mesh=mesh, name='Uy', value=0.0, hasOld=True)

u.constrain(0.0, mesh.facesRight | mesh.facesLeft | mesh.facesBottom)

u.constrain(lid, mesh.facesTop)
v.constrain(0.0, mesh.exteriorFaces)

#p.faceGrad.constrain(0.0, mesh.exteriorFaces)

# stokes flow
stokesu = TransientTerm(coeff=rho,var=u) + u*u.grad.dot([1.0,0.0]) + v*u.grad.dot([0.0,1.0]) == -p.grad.dot([1.0,0.0]) + DiffusionTerm(coeff=viscosity,var=u)
stokesv = TransientTerm(coeff=rho,var=v) + u*v.grad.dot([1.0,0.0]) + v*v.grad.dot([0.0,1.0]) == -p.grad.dot([0.0,1.0]) + DiffusionTerm(coeff=viscosity,var=v) 

# continuity equation for incompressible flow
continuity = u.grad.dot([1.0,0.0]) + v.grad.dot([0.0,1.0]) == 0.0

viewer = Viewer(vars=(p, u, v),
                xmin=0., xmax=1., ymin=0., ymax=1., colorbar=True)

dt = 0.01
t = 0.0

for j in range(100):

    t += dt
    # advances timestep
    p.updateOld()
    u.updateOld()
    v.updateOld()

    res = 1000.0
    i = 0
    
    while res > 1.0E-3:
        res = eqns.sweep(dt=dt)
        i += 1

        # if i%5 == 0:
        print i, 'res:',res

    #if j%10 == 0:        
    viewer.plot()

