import mpmath as mp
import numpy as np
import matplotlib.pyplot as plt

alpha = 1.0
gamma = 1.0
beta1 = 1.0
beta2 = 1.0
beta3 = 0.0
cinf = 0.001

theta = 62.2
eta = 3.3
sinf = 0.001
kappa = 1.0
spe = 0.001

# this appears twice
arg = lambda c,s : (1.0 + beta1*c + beta2*c**2 + beta3*c**3)*(1.0 - s)

# function to integrate, takes a vector y argument, returns a vector y result
# x is time (which doesn't appear in equations), but the result is y'_0 and y'_1, which are the time derivatives (LHS)
f = lambda x,y :[alpha*gamma*arg(y[0],y[1]) - (y[0] - cinf),
                 (gamma*arg(y[0],y[1]) - eta*(y[1] - sinf) - kappa*(y[1] - spe))/theta]

# this returns a function that solves the system of equations
# second argument is x_0 (here t_0)
# third argument is y_0(x_0) and y_1(x_0)  (here c and s)
g = mp.odefun(f, 0.0, [0.0, 0.0])

mp.mp.dps = 15
mp.mp.pretty = True

results = []

for t in np.linspace(0.0,50.0,1000):
    val = [t]
    val.extend(g(t))
    results.append(val)
    print val

results = np.array(results)

plt.subplot(211)
plt.plot(results[:,0],results[:,1],'r-',label='c')
plt.title('c(t)')
plt.subplot(212)
plt.plot(results[:,0],results[:,2],'g-',label='s')
plt.title('s(t)')
plt.savefig('glass_mpmath.png')

