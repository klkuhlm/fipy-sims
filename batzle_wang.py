# implement EOS for density and viscosity according to Adams & Bachu (2002) with erratum

import numpy as np

# Batzle & Wang (1992), as Described in Adams & Bachu (2002).
def water_density(T, P):
    """equation 10 of A&B (2002)
    T in degrees C
    P in Pa (_NOT_ MPa)
    return: rho_w in kg/m^3 (_NOT_ g/cm^3)
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3
    """

    T2 = T*T
    T3 = T2*T
    P2 = P*P
    
    return (1.0E+3 + 1.0E-3*(-80.0*T - 3.3*T2 + 0.00175*T3 +
                             4.89E-4*P - 2.0E-6*T*P + 1.6E-8*T2*P - 1.3E-11*T3*P -
                             3.33E-13*P2 - 2.0E-15*T*P2))

def drhow_dp(T, P):
    """derivative of density of fresh water wrt pressure
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3

    d/dp 1000.+0.001 (-80 T-3.3 T^2+0.00175 T^3+4.89*10^(-4) p-2*10^(-6) T p+1.6*10^(-8) T^2 p-1.3*10^(-11) T^3 p-3.33*10^(-13) p^2-2*10^(-15) T p^2) = 
    p (-4.*10^-18 T-6.66*10^-16)-1.3*10^-14 T^3+1.6*10^-11 T^2-2.*10^-9 T+4.89*10^-7
    """

    T2 = T*T
    T3 = T2*T
    
    return 4.89E-7 - P*(4.0E-18*T + 6.66E-16) - 1.3E-14*T3 + 1.6E-11*T2 - 2.0E-9*T

def drhow_dT(T, P):
    """derivative of density of fresh water wrt temperature
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3

    d/dT 1000.+0.001 (-80 T-3.3 T^2+0.00175 T^3+4.89*10^(-4) p-2*10^(-6) T p+1.6*10^(-8) T^2 p-1.3*10^(-11) T^3 p-3.33*10^(-13) p^2-2*10^(-15) T p^2) = 
    -2.*10^-18 p^2+p (-3.9*10^-14 T^2+3.2*10^-11 T-2.*10^-9)+5.25*10^-6 T^2-0.0066 T-0.08
    """
    
    T2 = T*T

    return -2.0E-18*P**2 + P*(-3.9E-14*T2 + 3.2E-11*T - 2.0E-9) + 5.25E-6*T2 - 0.0066*T - 0.08

def drhob_dp(T, P, S):
    """derivative of density of brine wrt pressure 
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3

    d/dp S (0.668+0.44 S+1*10^(-6) (3*10^(-4) p-2.4*10^(-3) p S+T (80+3 T-3300.0 S-1.3*10^(-5) p+4.7*10^(-5) p S)))*1*10^3 = 
    4.7*10^-8 S (S (T-51.0638)-0.276596 T+6.38298)
    """
    
    drhow_dp_val = drhow_dp(T,P)

    return drhow_dp_val + 4.7E-8*S*(S*(T - 51.0638) - 0.276596*T + 6.38298)

def drhob_dT(T, P, S):
    """derivative of density of brine wrt temperature 
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3

    d/dT S (0.668+0.44 S+1*10^(-6) (3*10^(-4) p-2.4*10^(-3) p S+T (80+3 T-3300.0 S-1.3*10^(-5) p+4.7*10^(-5) p S)))*1*10^3 = 
    0.001 S (p (0.000047 S-0.000013)-3300 S+6 T+80)
    """
    
    drhow_dT_val = drhow_dT(T,P)

    return drhow_dT_val + 1.0E-3*S*(P*(4.7E-5*S - 1.3E-5) - 3300.0*S + 6.0*T + 80.0)

def drhob_dS(T, P, S):
    """derivative of density of brine wrt salinity
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3

    d/dS S (0.668+0.44 S+1*10^(-6) (3*10^(-4) p-2.4*10^(-3) p S+T (80+3 T-3300.0 S-1.3*10^(-5) p+4.7*10^(-5) p S)))*1*10^3 = 
    p (S (9.4*10^-8 T-4.8*10^-6)-1.3*10^-8 T+3.*10^-7)+S (880-6.6 T)+0.003 T^2+0.08 T+668
    """

    return P*(S*(9.4E-8*T - 4.8E-6) - 1.3E-8*T + 3.0E-7) + S*(880.0 - 6.6*T) + 0.003*T**2 + 0.08*T + 668.0

def brine_density(T, P, S):
    """equation 11 of A&B (2002)
    T in degrees C
    P in Pa (_NOT_ MPa)
    S in ppm/10^6 (NaCl mass fraction)
    return: rho_B in kg/m^3 (_NOT_ g/cm^3)
    posed directly in terms of Pa and kg/m^3, rather than MPa and g/cm^3"""

    rhow = water_density(T,P)
    
    return rhow + 1.0E+3*(S*(0.668 + 0.44*S + 1.0E-6*(3.0E-4*P - 2.4E-3*P*S +
                  T*(80.0 + 3.0*T - 3300.0*S - 1.3E-5*P + 4.7E-5*P*S))))

def viscosity(T, S):
    """equation 19 of A&B (2002) -- w/ erratum
    T in degrees C
    P dependence not included
    S in ppm/10^6
    return: mu_B in Pa-sec (_NOT_ cP)"""

    A = (0.42*(np.power(S,0.8) - 0.17)**2 + 0.045)*np.power(T,0.8)
    
    return (0.1 + 0.333*S + (1.65 + 91.9*S**3)*np.exp(-A))*1.0E-3


