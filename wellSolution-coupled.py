from fipy import CylindricalGrid1D, CellVariable, TransientTerm, DiffusionTerm, Viewer
import numpy as np

L = 50.0
nr = 100
dr = L/nr
#timeStep = 0.001 # dx/10.0
steps = 100

Ss = 2.0E-4 # 1/m
Kr = 4.5E-4 # m/s
b = 16 # m
S = Ss*b
T = Kr*b
#Sel = 1.0E-8

Q = -1.0E0

sigma = 4.0E-3 # S/m  
C = -10.00 # mV/m

m = CylindricalGrid1D(dx=dr, nx=nr)
m.origin = (1.0E-4) # wellbore radius

s = CellVariable(name='drawdown',mesh=m, value=0.0)
phi = CellVariable(name='voltage',mesh=m, value=0.0)

well = Q/(2.0*np.pi*T)

# hydraulic BC
s.faceGrad.constrain(well, m.facesLeft)
s.constrain(0.0,  m.facesRight)

# electric BC
phi.faceGrad.constrain(-well*C, m.facesLeft)
phi.constrain(0.0,     m.facesRight)

# governing equations
eqgw = (DiffusionTerm(T,var=s) == TransientTerm(S,var=s) + 
        TransientTerm(C*sigma,var=phi))
eqel = (DiffusionTerm(sigma,var=phi) == TransientTerm(1.0E-6,var=phi) + 
        TransientTerm(-C*sigma,var=s)) 

# coupled
eq = eqgw & eqel

viewer1 = Viewer(vars=(s,phi))
viewer1.plot()

fhs = open('solution-s.dat','w')
fhe = open('solution-e.dat','w')

t=0

for step in np.logspace(-3,2,steps):
    t += step
    eq.solve(dt=step)
    print t
    viewer1.plot()
    out = (t,s.getValue()[1],s.getValue()[10],
           s.getValue()[20],s.getValue()[-20])
    fhs.write('%.5e,%.5e,%.5e,%.5e,%.5e\n' % out)
    out = (t,phi.getValue()[1],phi.getValue()[10],
           phi.getValue()[20],phi.getValue()[-20])
    fhe.write('%.5e,%.5e,%.5e,%.5e,%.5e\n' % out)

fhs.close()
fhe.close()
