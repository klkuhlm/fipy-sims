from fipy import *

L = 3.0 # radial length of domain (m)
b = 0.25 # thickness of a layer (m)
nr = 30
dr = L/nr

nz = 15
dz = b/nz

timeStep = 0.01 # dx/10.0
steps = 100

Ss = 2.0E-4 # 1/m
Kr = 4.5E-4 # m/s
Sy = 0.3
S = Ss*b
T = Kr*b

Q = 1.0E0

mesh = CylindricalGrid2D(dx=dr, nx=nr, ny=nz, dy=dz)
mesh.origin = array([[1.0E-4],[0.0]])

s = CellVariable(name='drawdown',mesh=mesh, value=0.0)

pumpingRate = Q/(2.0*pi*T)

gwBCs = (FixedFlux(mesh.getFacesLeft(),pumpingRate),
         FixedValue(mesh.getFacesRight(),0.0),
         )

gw = TransientTerm(S) == DiffusionTerm(T)

viewer1 = Viewer(vars=(s,))
viewer1.plot()

time = 0.0

for step in range(steps):
    time += timeStep

    print step,time

    gw.solve(s, dt=timeStep, boundaryConditions=gwBCs)
    
