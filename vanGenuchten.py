class vanGenuchten:
    # class to save parameters and functions in

    def __init__(self, m, alpha):
        self.m = m
        self.alpha = alpha
        self.n = 1.0 / (1.0 - m)
        self.p = 0.5

    def calc_C(self, pc):
        # capacity function 
        pcs = self.alpha * pc # scaled pc
        return (
            self.n
            * self.m
            * self.alpha
            * pcs ** (self.n - 1)
            / (1.0 + pcs **self.n) ** (self.m + 1)
        )

    def calc_Se(self, pc):
        # saturation function
        return (1.0 + (self.alpha * pc) ** self.n) ** (-self.m)

    def calc_Kr(self, se):
        # relative hydraulic conductivity 
        return se**self.p * (1.0 - (1.0 - se ** (1.0 / self.m)) ** self.m) ** 2
