import numpy as np
import matplotlib.colors as mpc
import matplotlib.pyplot as plt
from glob import glob

driftFloor = 60.0

FT2M = 0.3048

#nx = 30*2+1  # not sure why uniform grid seems to have 1 extra term in x-direction
#ny = 28*2
#nz = 70*2

nx = 55
ny = 36
nz = 82

power = 500

def plot_x_xsec_geom(xval):

    x = 30-xval

    # plot box for drift (y,z)
    plt.plot([27.5,20,20,27.5,27.5],
             [driftFloor,driftFloor,driftFloor+10,driftFloor+10,driftFloor],'k-',linewidth=2)

    # 0 <= x <= 9 is main pile
    if x <= 9.0:
        # line across top of pile
        plt.plot([27.5,20],[driftFloor+6,driftFloor+6],'k-')

        # heaters
        if x <= 1.0 or (x >= 2.0 and x <= 4.0) or (x >= 5.0 and x <= 7.0):
            plt.plot([27.5,27.5,22.5,22.5],[driftFloor,driftFloor+2,driftFloor+2,driftFloor],'k-')

    # 9 < x <= 17.5 is slope on end of pile (36 degree slope)
    elif x <= 17.5:
        dx = x - 9.0
        plt.plot([27.5,20],[driftFloor+6-(dx*6/7.0),driftFloor+6-(dx*6/7.0)],'k-')
        

def plot_y_xsec_geom(yval):
    
    y = 27.5-yval

    if y < 7.5:
        # plot box for drift (x,z)
        plt.plot([0,30,30,0,0],
                 [driftFloor,driftFloor,driftFloor+10,driftFloor+10,driftFloor],'k-',linewidth=2)
        
        # plot top of salt pile
        plt.plot([30,21,12.5],[driftFloor+6,driftFloor+6,driftFloor],'k-')

        # plot heater cross-sections
        th = np.linspace(0,2*np.pi)
        plt.plot(30-np.cos(th),np.sin(th)+driftFloor+1,'k-')
        plt.plot(27-np.cos(th),np.sin(th)+driftFloor+1,'k-')
        plt.plot(24-np.cos(th),np.sin(th)+driftFloor+1,'k-')

#def plot_z_xsec_geom(zval):
#
#    if z >= 30 and z <= 40:
#        plt.plot([],[],'')

tvals = [16.57,36.97,102,342,702,742]

print nx,ny,nz,nx*ny*nz

for tval in tvals:
    
    print 't=',tval

    x,y,z,T = np.loadtxt('temp-nonlinear-%iW-nonuniform-%.4g.tsv' % (power,tval),skiprows=2,unpack=True)

    # convert distances to feet
    x /= FT2M
    y /= FT2M
    z /= FT2M
    
    Tmin = T.min()
    Tmax = T.max()
    
    # normalize contours on all slices to min/max for whole dataset
    nrm = mpc.Normalize(vmin=Tmin,vmax=Tmax)

    # uniform grid is saved in C style,
    # while nonuniform grid is staved in F style (huh?)

    x = x.reshape((nx,ny,nz),order='F')
    y = y.reshape((nx,ny,nz),order='F')
    z = z.reshape((nx,ny,nz),order='F')
    T = T.reshape((nx,ny,nz),order='F')

    xvec = x[:,0,0]
    yvec = y[0,:,0]
    zvec = z[0,0,:]

    print 'x',xvec.shape,x.shape
    print xvec
    print 'y',yvec.shape,y.shape
    print yvec
    print 'z',zvec.shape,z.shape
    print zvec

    # x coordinate is zero at bulkhead

    cvec = [25+(v*5.0) for v in range(int(np.ceil((Tmax-Tmin)/5))+1)]

    for xidx in [0,15,30,40,45,50,nx-1]:

        print xidx

        xx = xvec[xidx] 

        plt.figure(1,figsize=(11,17))
        plt.subplot(111)
        CS = plt.contour(y[xidx,:,:],z[xidx,:,:],T[xidx,:,:],cvec,norm=nrm)
        # plt.colorbar()
        plt.clabel(CS, fontsize=9, inline=1, fmt='%.0f')
        plt.axis('image')
        plt.xlim((0,27.5))
        plt.ylim((driftFloor-40,driftFloor+50)) # 40 ft above and below drift
        plt.xlabel('Y [ft]')
        plt.ylabel('Z [ft]')
        plot_x_xsec_geom(xx)
        plt.grid()
        plt.title('uniform scale t=%.3g d, x=%.3g' % (tval,xx),fontsize='small')

        plt.tight_layout()
        plt.savefig('temp-%iW-t%.3g-x%.3g.pdf' % (power,tval,xx))
        plt.close(1)

    for yidx in [0,15,25,30,ny-1]:

        print yidx

        yy = yvec[yidx]
        plt.figure(1,figsize=(11,17))
        plt.subplot(111)
        CS = plt.contour(x[:,yidx,:],z[:,yidx,:],T[:,yidx,:],cvec,norm=nrm)
        plt.clabel(CS, fontsize=9, inline=1, fmt='%.0f')
        #plt.colorbar()
        plt.axis('image')
        plt.xlim((0,30))
        plt.ylim((driftFloor-40,driftFloor+50)) # 40 ft above and below drift
        plt.xlabel('X [ft]')
        plt.ylabel('Z [ft]')
        plot_y_xsec_geom(yy)
        plt.grid()
        plt.title('uniform scale t=%.3g d, y=%.3g' % (tval,yy),fontsize='small')


        plt.tight_layout()
        plt.savefig('temp-%iW-t%.3g-y%.3g.pdf' % (power,tval,yidx))
        plt.close(1)
