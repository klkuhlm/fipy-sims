from fipy import Grid1D, CellVariable, TransientTerm, Viewer
import numpy as np
import matplotlib.pyplot as plt

m = Grid1D(dx=1.0, nx=1)

c = CellVariable(name='c', mesh=m, value=0.0, hasOld=True)
s = CellVariable(name='s', mesh=m, value=0.0, hasOld=True)

alpha = 1
gamma = 1
beta1 = 1
beta2 = 40
cinf = 0.001

theta = 60
eta = 1
sinf = 0.001
kappa = 1
spe = 0.001

eq1 = (TransientTerm(1.0,var=c) == alpha*gamma*(1+ beta1*c + beta2*c*c)*(1-s)- (c - cinf))
eq2 = (TransientTerm(theta,var=s) == gamma*(1+ beta1*c + beta2*c*c)*(1-s) - eta*(s - sinf) - kappa*(s - spe))

eq = eq1 & eq2

#viewer = Viewer(vars=(c,s))

t=0
dt = 0.0003

fh = open('test.out','w')

for j in range(2000):
    t += dt
    
    c.updateOld()
    s.updateOld()

    res = 1000.0
    iteration = 0
    
    while res > 1.0E-8:
        res = eq.sweep(dt=dt)
        print iteration,res
        iteration += 1
        
    fh.write('%i %e %e %e\n' % (j,t,s.value[0],c.value[0]))
    print j,t,s.value,c.value
    #viewer.plot()

fh.close()

idx,tt,ss,cc = np.loadtxt('test.out',unpack=True)
plt.plot(tt,ss,'r-')
plt.plot(tt,cc,'b--')
plt.xlabel('t')
plt.ylabel('c,s')
plt.savefig('glass.png')
