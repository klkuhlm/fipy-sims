r"""
Stokes equations for incompressible fluid flow.
"""
from sfepy import data_dir

mesh = 0

if mesh == 0:
    filename_mesh = '/home/klkuhlm/repos/fipy-sims/channel.mesh'
    regions = {'Omega' : 'all',
               'Inlet' : ('vertices in (x < 0.00001)', 'facet'),
               'Outlet' : ('vertices in (x > 0.003999)', 'facet'),
               'Symmetry' : ('vertices in (y < 0.00001)', 'facet'),
               'AllEdges' : ('vertices of surface', 'facet'),
               'Walls' : ('r.AllEdges -s (r.Inlet +s r.Outlet +s r.Symmetry)', 'facet')}
    
    ebcs = {'u1' : ('Walls', {'u.all' : 0.0}),
            'u2' : ('Inlet', {'u.0' : 1.0}),
            'u3' : ('Symmetry', {'u.1' : 0.0})}

elif mesh == 1:
    filename_mesh = data_dir + '/meshes/3d/cylinder.mesh'
    regions = {'Omega' : 'all',
               'Inlet' :  ('vertices in (x < 0.00001)', 'facet'),
               'Outlet' : ('vertices in (x > 0.099999)', 'facet'),
               'AllEdges' : ('vertices of surface', 'facet'),
               'Walls' : ("r.AllEdges -s (r.Inlet +s r.Outlet)", 'facet')}

    ebcs = {'u1' : ('Walls', {'u.all' : 0.0}),
            'u2' : ('Inlet', {'u.0' : 1.0})}
    
elif mesh == 2:
    filename_mesh = data_dir + '/meshes/3d/elbow2.mesh'
    regions = {
        'Omega'   : 'all',
        'AllEdges' : ('vertices of surface', 'facet'),
        'Walls'  : ('r.AllEdges -v r.Ends', 'facet'),
        'Ends' :  ('vertices in (y < 0.002)', 'facet'),
        'Inlet'  : ('r.Ends *v vertices in (x < 0.1)', 'facet'),
        'Outlet' : ('r.Ends *v vertices in (x > 0.1)', 'facet')}

    ebcs = {'u1' : ('Walls', {'u.all' : 0.0}),
            'u2' : ('Input', {'u.1' : 1.0})}
    
    
fields = {
    'velocity' : ('real', 'vector', 'Omega', 2),
    'pressure' : ('real', 'scalar', 'Omega', 1)}

variables = {
    'u' : ('unknown field', 'velocity', 0),
    'v' : ('test field', 'velocity', 'u'),
    'p' : ('unknown field', 'pressure', 1),
    'q' : ('test field', 'pressure', 'p')}

equations = {
    'balance' :
    "dw_div_grad.5.Omega( fluid.viscosity, v, u ) - dw_stokes.5.Omega( v, p ) = 0",
    
    'incompressibility' :
    "dw_stokes.5.Omega( u, q ) = 0"}

materials = {'fluid' :  ({'viscosity' : 1.0, 'density' : 1.0},)}

solvers = {'ls' : ('ls.scipy_direct', {}),
    'newton'  : ('nls.newton', {
    'i_max'      : 2,
    'eps_a'      : 1e-8,
    'eps_r'      : 1e-2,
    'macheps'   : 1e-16,
    'lin_red'    : 1e-2, 
    'ls_red'     : 0.1,
    'ls_red_warp' : 0.001,
    'ls_on'      : 1.1,
    'ls_min'     : 1e-5,
    'check'     : 0,
    'delta'     : 1e-6,
    'problem'   : 'nonlinear', # 'nonlinear' or 'linear' (ignore i_max)
    })}

save_format = 'vtk' # 'hdf5' or 'vtk'
