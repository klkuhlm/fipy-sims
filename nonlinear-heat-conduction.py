from fipy import Grid1D, CellVariable, TransientTerm, DiffusionTerm, Viewer
import numpy as np

L = 5.0  
nx = 100
dx = L/nx
timeStep = 0.5 
steps = 300

linear = False

# thermal conductivity [W/(m deg-C)] parameters
# kappa = a0 + a1*T + a2*T**2 + a3*T**3

a = [5.734, -1.838E-2, 2.86E-5, -1.51E-8]

# volumetric heat capacity: [J/(m^3 deg-C)]
# c_th,rs*rho_rs = b0 + b1*T 

b = [1.8705E+6, 3.8772E+2]

m = Grid1D(dx=dx, nx=nx)
T = [CellVariable(name='linear T',mesh=m, value=0.0),
     CellVariable(name='nonlinear T',mesh=m, value=0.0, hasOld=1)]

Q = CellVariable(name='source',mesh=m, value=0.0)
x = m.cellCenters[0]
Q.setValue(-100.0, where=(x>1.5)&(x<2.5))

# case 1) constant temp BC on left
# thermal BC (specified temp at x=0) 
T[0].constrain(200.0, m.facesLeft)
T[1].constrain(200.0, m.facesLeft)

# case 2) constant flux BC on left
# thermal BC (specified flux at x=0) 
#T[0].faceGrad.constrain(-1.0E1, m.facesLeft)
#T[1].faceGrad.constrain(-1.0E1, m.facesLeft)

# specified temp =0 at x=L
T[0].constrain(0.0, m.facesRight)
T[1].constrain(0.0, m.facesRight)

# insulated boundary condition on right 
#T[0].faceGrad.constrain(0.0, m.facesRight)
#T[1].faceGrad.constrain(0.0, m.facesRight)

# heat conduction governing equation
eq = [TransientTerm(b[0], var=T[0]) == DiffusionTerm(a[0], var=T[0]) + Q,    
      (TransientTerm(b[0] + b[1]*T[1], var=T[1]) == 
       DiffusionTerm(a[0] + a[1]*T[1] + a[2]*T[1]**2 + a[3]*T[1]**3, var=T[1]) + Q)]

viewer = Viewer(vars=(T[0],T[1]),datamin=-5,datamax=200)
viewer.plot()

t = 0.0

for step in np.logspace(3,6,steps):
    t += step
    eq[0].solve(dt=step)
    T[1].updateOld()

    res = 1.0E+10
    while res > 1.0E-4:
        #print 'res',res
        res = eq[1].sweep(dt=step)
   
    print t

    viewer.plot()


