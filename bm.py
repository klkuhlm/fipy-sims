from fipy import CellVariable, FaceVariable, TransientTerm, DiffusionTerm, Viewer, Grid2D
import numpy as np
import matplotlib.pyplot as plt

dx=0.25
dy=0.25
nx=50
ny=100

mesh = Grid2D(dx=dx, dy=dy, nx=nx, ny=ny)

p = CellVariable(name='pressure', mesh=mesh, value=0.0)
sxx = CellVariable(name='stress_xx', mesh=mesh, value=0.0)
szz = CellVariable(name='stress_zz', mesh=mesh, value=0.0)
sxz = CellVariable(name='stress_xz', mesh=mesh, value=0.0)

