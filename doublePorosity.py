from fipy import *
import matplotlib.pyplot as plt
import numpy as np

L = 10.0
nx = 100
dx = L/nx
timeStep = dx/10.0

steps = 150
phim = 0.10   # mobile zone porosity
phiim = 0.05  # immobile zone porosity
beta = 0.05    # mobile/immobile domain transfer rate
D = 1.0E-1    # mobile domain diffusion coeff
Rm = 1.0     # mobile domain retardation coefficient
Rim = 1.0    # immobile domain retardation coefficient

betaT = phiim*Rim/(phim*Rm)
DR = D/Rm

plotBreakthrough = False

mesh = Grid1D(dx=dx, nx=nx)
c0 = np.zeros(nx, 'd')
c0[20:50] = 1.0

# mobile domain concentration
cm = CellVariable(name="$c_m$", mesh=mesh, value=c0)

# immobile domain concentration
cim = CellVariable(name="$c_{im}$", mesh=mesh, value=0.0, hasOld=True)

# zero concentration flux boundary conditions
BCs = (FixedFlux(mesh.getFacesLeft(), 0.0),
       FixedValue(mesh.getFacesRight(), 0.0))

# advective flow velocity 
u = FaceVariable(mesh=mesh, value=(0.0,), rank=1)

# 1D convection diffusion equation (mobile domain)
# version with \frac{\partial c_{im}}{\partial t}
eqM = (TransientTerm(1.0) + betaT*(cim - cim.getOld())/timeStep == 
       DiffusionTerm(DR) - ExponentialConvectionTerm(u/(Rm*phim)))

# immobile domain (lumped approach)
eqIM = TransientTerm(Rim*phiim) == beta/Rim*(cm - ImplicitSourceTerm(1.0))

viewer = Viewer(vars=(cm,cim), datamin=0.0, datamax=1.0)
viewer.plot()

if plotBreakthrough:
    fig = plt.figure()
    axlin = fig.add_subplot(311)
    axlog = fig.add_subplot(312)
    axll = fig.add_subplot(313)
time = 0.0

fh = open('breakthrough.csv','w')

for step in range(steps):
    time += timeStep

    if time < 0.5:
        u.setValue((1.0,))
    elif time < 1.0:
        u.setValue((0.0,))
    else:
        u.setValue((-1.0,))

    cim.updateOld()
    eqIM.solve(cim, dt=timeStep, boundaryConditions=BCs)
    eqM.solve(cm,  dt=timeStep, boundaryConditions=BCs)
    breakthrough = cm.getValue()[-1]
    if plotBreakthrough:
        axlin.plot(time,breakthrough,'b_')
        axlog.semilogy(time,breakthrough,'b_')
        axll.loglog(time,breakthrough,'b_')
        plt.show()
    viewer.plot()
    fh.write('%.3f,%.5g\n' % (time,breakthrough))
    
fh.close()

